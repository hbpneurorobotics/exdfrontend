(function() {
  'use strict';
  angular
    .module('experimentSharingServiceMock', [])
    .service('experimentSharingService', function() {
      this.launchSharedEntityWindow = jasmine.createSpy(
        'launchSharedEntityWindow'
      );
    });
})();
