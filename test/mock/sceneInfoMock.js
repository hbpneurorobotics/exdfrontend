(function() {
  'use strict';

  angular.module('sceneInfoMock', []).service('sceneInfo', [
    '$q',
    function($q) {
      this.robots = [
        { robotId: 'robot' },
        { robotId: 'icub' },
        { robotId: 'husky' }
      ];
      this.refreshRobotsList = $q.when();
      this.initialized = $q.when();
      this.isRobot = jasmine.createSpy('isRobot');
      this.isCustom = jasmine
        .createSpy('isCustom')
        .and.callFake(function(robot) {
          return robot && robot.isCustom === true;
        });
      this.isShared = jasmine
        .createSpy('isShared')
        .and.callFake(function(robot) {
          return robot && robot.isShared === true;
        });
    }
  ]);
})();
