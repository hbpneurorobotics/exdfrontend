(function() {
  'use strict';
  angular
    .module('ModelSharingServiceMock', [])
    .service('ModelSharingService', function() {
      this.launchSharedEntityWindow = jasmine.createSpy(
        'launchSharedEntityWindow'
      );
    });
})();
