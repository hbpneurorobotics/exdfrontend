'use strict';
(function() {
  angular
    .module('storageServerMock', [])
    .service('storageServer', [
      '$q',
      function($q) {
        var currentUser = (this.currentUser = {
          id: 'theUserID',
          displayName: 'theOwnerName'
        });

        const robotModels = [
          {
            name: 'robot1_name',
            displayName: 'robot1 name',
            sdf: 'model.sdf',
            description: 'robot1 description',
            thumbnail: '<robot png data>',
            id: 'robot1',
            configPath: 'robot1_name/model.config',
            isCustom: true,
            type: 'robots',
            path: 'robot1_name'
          },
          {
            name: 'robot2_name',
            displayName: 'robot2 name',
            sdf: 'model.sdf',
            thumbnail: '<robot png data>',
            id: 'robot1',
            configPath: 'robot2_name/model.config',
            isCustom: false,
            type: 'robots',
            path: 'robot2_name'
          },
          {
            configPath: 'pioneer_3dx/model.config',
            description: 'A ROS/Gazebo Pioneer 3DX model.',
            id: 'p3dx',
            name: 'pioneer_3dx',
            displayName: 'Pioneer 3DX',
            path: 'pioneer_3dx',
            sdf: 'p3dx.sdf',
            thumbnail: 'thumbnail.png',
            isCustom: true,
            type: 'robots'
          }
        ];

        const brainModels = [
          {
            name: 'brain1_name',
            displayName: 'brain1 name',
            description: 'brain1 description',
            path: 'brain1_name',
            type: 'brains',
            isCustom: false
          },
          {
            name: 'brain2_name',
            displayName: 'brain2 name',
            path: 'brain2_name',
            type: 'brains',
            isCustom: true
          }
        ];

        const environmentsModels = [
          {
            name: 'env1_name',
            displayName: 'env1 name',
            description: 'env1 description',
            path: 'env1_name',
            type: 'environments',
            isCustom: false
          }
        ];

        this.getCurrentUser = jasmine
          .createSpy('storageServerMock.getCurrentUser')
          .and.callFake(function() {
            return $q.when(currentUser);
          });

        this.setCustomModel = jasmine
          .createSpy('storageServerMock.setCustomModel')
          .and.returnValue($q.resolve());

        this.deleteCustomModel = jasmine
          .createSpy('storageServerMock.deleteCustomModel')
          .and.returnValue($q.resolve());

        this.getUser = jasmine
          .createSpy('storageServerMock.getUser')
          .and.callFake(function() {
            return $q.when(currentUser);
          });

        this.getCurrentUserGroups = jasmine
          .createSpy('storageServerMock.getCurrentUserGroups')
          .and.callFake(function() {
            return $q.when([{ name: 'hbp-sp10-user-edit-rights' }]);
          });

        this.cloneTemplate = jasmine
          .createSpy('storageServerMock.cloneTemplate')
          .and.callFake(function() {
            return $q.when([{ name: 'hbp-sp10-user-edit-rights' }]);
          });

        this.getTransferFunctions = jasmine
          .createSpy('getTransferFunctions')
          .and.returnValue(
            $q.resolve({
              data: { tf1: 'pass', tf2: 'pass', faultyTf: 'invalidCode' },
              active: { tf1: true, tf2: true, faultyTf: false }
            })
          );

        this.deleteFile = jasmine
          .createSpy('storageServerMock.deleteFile')
          .and.returnValue($q.resolve());

        this.saveTransferFunctions = jasmine
          .createSpy('saveTransferFunctions')
          .and.returnValue($q.resolve());

        this.getRobotConfigPath = jasmine
          .createSpy('storageServerMock.getRobotConfigPath')
          .and.callFake(function() {
            return $q.when('robotpath');
          });

        var data = {};
        for (var i = 0; i < 3; ++i) {
          var smId = 'SM' + i;
          data[smId] = 'class ' + smId + '(DefaultStateMachine):\n';
        }
        this.getStateMachines = jasmine
          .createSpy('storageServerMock.getStateMachines')
          .and.callFake(() => window.$q.when({ data: data }));

        this.saveStateMachines = jasmine
          .createSpy('storageServerMock.saveStateMachines')
          .and.callFake(() => window.$q.when());

        this.getFileContent = jasmine.createSpy('getFileContent');
        this.cloneNew = jasmine
          .createSpy('storageServerMock.cloneNew')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when('robotpath');
            else return $q.reject({ data: 'Custom Model Failed' });
          });

        this.saveStateMachines = jasmine
          .createSpy('storageServerMock.saveStateMachines')
          .and.callFake(() => window.$q.when());
        this.setCustomModel = jasmine
          .createSpy('storageServerMock.setCustomModel')
          .and.callFake(function() {
            if (this.setItWasSuccessful) {
              return $q.resolve();
            } else {
              return $q.reject({ data: 'Custom Model Failed' });
            }
          });
        this.getSharedModelMode = jasmine
          .createSpy('storageServerMock.getSharedModelMode')
          .and.callFake(function() {
            if (this.itWasSuccessful)
              return $q.when({ sharingOption: 'Private' });
            else return $q.reject();
          });

        this.getSharedExpMode = jasmine
          .createSpy('storageServerMock.getSharedExpMode')
          .and.callFake(function() {
            if (this.itWasSuccessful)
              return window.$q.resolve({ data: 'Private' });
            else return $q.reject();
          });

        this.getCustomModelsByUser = jasmine
          .createSpy('storageServerMock.getCustomModelsByUser')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.resolve(robotModels);
            else return $q.reject();
          });

        this.getAllModels = jasmine
          .createSpy('storageServerMock.getAllModels')
          .and.callFake(type => {
            if (type === 'brains') {
              return $q.resolve(brainModels);
            } else if (type === 'robots') {
              return $q.resolve(robotModels);
            } else if (type === 'environments') {
              return $q.resolve(environmentsModels);
            }
          });

        this.getCustomSharedModels = jasmine
          .createSpy('storageServerMock.getCustomSharedModels')
          .and.callFake(function() {
            return $q.when(this.modelFakeArray);
          });

        this.getModelsbyType = jasmine
          .createSpy('storageServerMock.getModelsbyType')
          .and.returnValue($q.resolve(robotModels));

        this.getAllUsers = jasmine
          .createSpy('storageServerMock.getAllUsers')
          .and.callFake(function() {
            return $q.when([]);
          });

        this.updateSharedExpMode = jasmine
          .createSpy('storageServerMock.updateSharedExpMode')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when(this.modelFakeArray);
            else return $q.reject();
          });

        this.updateSharedModelMode = jasmine
          .createSpy('storageServerMock.updateSharedModelMode')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when();
            else return $q.reject();
          });

        this.deleteSharedExpUser = jasmine
          .createSpy('storageServerMock.deleteSharedExpUser')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when();
            else return $q.reject();
          });

        this.deleteSharedModelUser = jasmine
          .createSpy('storageServerMock.deleteSharedModelUser')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when();
            else return $q.reject();
          });

        this.addSharedExpUsers = jasmine
          .createSpy('storageServerMock.addSharedExpUsers')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when();
            else return $q.reject();
          });

        this.addSharedModelUsers = jasmine
          .createSpy('storageServerMock.addSharedModelUsers')
          .and.callFake(function() {
            if (this.itWasSuccessful) return $q.when();
            else return $q.reject();
          });

        this.addSharingUsers = jasmine
          .createSpy('storageServerMock.addSharingUsers')
          .and.callFake(function() {
            return $q.when('ok');
          });

        this.deleteSharingUser = jasmine
          .createSpy('storageServerMock.deleteSharingUser')
          .and.callFake(function() {
            if (this.itWasSuccessful) return window.$q.resolve();
            else return $q.reject();
          });

        this.getSharedExpUsers = jasmine
          .createSpy('storageServerMock.getSharedExpUsers')
          .and.callFake(function() {
            if (this.itWasSuccessful) return window.$q.resolve(['user0']);
            else return $q.reject();
          });

        this.getSharedModelUsers = jasmine
          .createSpy('storageServerMock.getSharedModelUsers')
          .and.callFake(function() {
            if (this.itWasSuccessful) return window.$q.resolve(['user0']);
            else return $q.reject();
          });

        this.getBase64Content = jasmine
          .createSpy('storageServerMock.getBase64Content')
          .and.callFake(function() {
            return $q.when([]);
          });

        this.modelFakeArray = [
          {
            id: 'id',
            name: 'name',
            modelType: 'modelType',
            fileName: 'fileName',
            isShared: 'isShared',
            description: 'description',
            thumbnail: 'thumbnail',
            path: 'path',
            script: 'script'
          }
        ];
        this.modelsByTypeArray = [
          {
            uuid: 'uuid',
            fileName: 'fileName',
            userId: 'userId'
          }
        ];
        this.getFileContent = jasmine
          .createSpy('getFileContent')
          .and.callFake(function() {
            var xmlVersionString = '<?xml version="1.0"?>';
            var xmlModelString = '<model>';
            var xmlNameString = '<name>iCub HBP ros</name>';
            var xmlDescritptionString =
              '<description>Model for the iCub humanoid robot. For more information check icub.org.</description>';
            var xmlModelTerminateString = '</model>';
            var xml = xmlVersionString
              .concat(xmlModelString)
              .concat(xmlNameString)
              .concat(xmlDescritptionString)
              .concat(xmlModelTerminateString);
            return $q.when({
              uuid: 'fakeuuid',
              data: xml
            });
          });
        this.setFileContent = jasmine.createSpy('setFileContent');
        this.getBrain = jasmine
          .createSpy('getBrain')
          .and.callFake(() => window.$q.resolve({}));
        this.getKnowledgeGraphBrains = jasmine
          .createSpy('getKnowledgeGraphBrains')
          .and.callFake(() => window.$q.resolve([]));
        this.saveBrain = jasmine
          .createSpy('saveBrain')
          .and.callFake(() => window.$q.resolve());
        this.reset = function() {
          this.getCurrentUser.calls.reset();
          this.getUser.calls.reset();
          this.getCurrentUserGroups.calls.reset();
        };

        this.logActivity = jasmine.createSpy('logActivity');
        this.itWasSuccessful = true;
        this.setItWasSuccessful = true;
        this.isGetAllWasSuccesful = true;

        // Import Experiment
        const importExperimentResponses = [
          {
            destFolderName: 'p3dx_3',
            zipBaseFolderName: 'p3dx'
          },
          {
            destFolderName: 'lauron_1',
            zipBaseFolderName: 'lauron'
          },
          {
            destFolderName: 'husky_holodeck_6',
            zipBaseFolderName: 'husky_holodeck'
          }
        ];
        this.importExperiment = jasmine
          .createSpy('storageServerMock.importExperiment')
          .and.returnValues(
            $q.resolve(importExperimentResponses[0]),
            $q.resolve(importExperimentResponses[1]),
            $q.resolve(importExperimentResponses[2])
          );
        const scanStorageResponse = {
          addedFolders: ['husky_sbc_11', 'cdp1_mouse_0', 'icub_dvs_7'],
          deletedFolders: ['p3dx_5', 'lauron_11', 'empty_husky_3']
        };
        this.scanStorage = jasmine
          .createSpy('storageServerMock.scanStorage')
          .and.returnValue($q.resolve(scanStorageResponse));
        this.saveKgAttachment = jasmine
          .createSpy('storageServerMock.saveKgAttachment')
          .and.returnValue($q.resolve());
      }
    ])
    .service('storageServerTokenManager', [
      function() {
        this.clearStoredToken = jasmine.createSpy('clearStoredToken');
        this.getStoredToken = jasmine.createSpy('getStoredToken');
      }
    ]);
})();
