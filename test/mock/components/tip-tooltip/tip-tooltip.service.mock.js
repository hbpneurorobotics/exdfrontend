(function() {
  'use strict';

  angular
    .module('tipTooltipServiceMock', [])
    .service('tipTooltipService', function() {
      this.setCurrentTip = jasmine.createSpy('setCurrentTip');
      this.loadLocalPreferences = jasmine.createSpy('loadLocalPreferences');
      this.reset = jasmine.createSpy('reset');
      this.setLocalTipPreferences = jasmine.createSpy('setLocalTipPreferences');
      this.tipToType = function() {
        return true;
      };
      this.tipCodes = {
        WELCOME: {
          text: 'Mock test',
          doNotShowAgainWhenMarkedAsRead: true
        },
        NAVIGATION: {
          text: 'Mock test',
          doNotShowAgainWhenMarkedAsRead: true
        },
        OBJECT_INSPECTOR: {
          text: 'Mock test.<br>' + 'Mock test.',
          doNotShowAgainWhenMarkedAsRead: true
        },
        OBJECT_LIBRARY_INFO: {
          text: 'Mock test',
          doNotShowAgainWhenMarkedAsRead: true
        }
      };
    });
})();
