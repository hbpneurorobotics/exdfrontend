(function() {
  'use strict';

  angular.module('uploadToKgServiceMock', []).service('uploadToKgService', [
    '$q',
    function($q) {
      const artifact = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10/artifacts/muscleforces/v1.0.0/25799a86-6ddf-43ce-9b9b-1ebf75c629a1',
        revision: 1,
        uploadedDate: '',
        distribution: [
          {
            downloadURL: 'url'
          }
        ]
      };

      this.simulation = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10/core/simulation/v1.0.0/9d4ea936-1426-4597-a9d6-9a3d669ae557',
        revision: 1
      };

      this.uploadSimulation = jasmine
        .createSpy('uploadSimulation')
        .and.returnValue($q.resolve());

      this.uploadArtifact = jasmine
        .createSpy('uploadArtifact')
        .and.returnValue($q.resolve(artifact));

      this.uploadAttachment = jasmine
        .createSpy('uploadAttachment')
        .and.callFake(() => {
          artifact.uploadedDate = '2019-12-27 13:12:60';
          return $q.resolve(artifact);
        });

      this.getUploadedDate = jasmine
        .createSpy('getUploadedDate')
        .and.returnValue($q.resolve(artifact.uploadedDate));
    }
  ]);
})();
