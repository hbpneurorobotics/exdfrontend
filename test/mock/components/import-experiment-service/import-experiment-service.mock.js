(function() {
  'use strict';

  angular
    .module('importExperimentServiceMock', [])
    .service('importExperimentService', function() {
      const importFolderResponse = {
        destFolderName: 'p3dx_5',
        zipBaseFolderName: 'p3dx'
      };
      const finalCallback = { finally: f => f() };
      this.importExperimentFolder = jasmine
        .createSpy('importExperimentFolder')
        .and.returnValue({
          then: f => {
            f(importFolderResponse);
            return finalCallback;
          },
          finally: f => {
            f();
          },
          catch: f => {
            f();
          }
        });
      const importZippedExperimentResponses = {
        destFolderName: 'lauron_0, husky_holodeck_1, p3dx_6',
        zipBaseFolderName: 'lauron, husky_holodeck, p3dx',
        numberOfZips: 3
      };
      this.importZippedExperiment = jasmine
        .createSpy('importZippedExperiment')
        .and.returnValue({
          then: f => {
            f(importZippedExperimentResponses);
            return finalCallback;
          },
          finally: f => {
            f();
          },
          catch: f => {
            f();
          }
        });
      const scanStorageResponse = {
        deletedFolders: 'lauron_0, husky_holodeck_1, p3dx_7',
        addedFolders: 'husky_holodeck_12, p3dx_7, lauron_0',
        deletedFoldersNumber: 3,
        addedFoldersNumber: 3
      };
      this.scanStorage = jasmine.createSpy('scanStorage').and.returnValue({
        then: f => {
          f(scanStorageResponse);
          return finalCallback;
        },
        finally: f => {
          f();
        },
        catch: f => {
          f();
        }
      });
    });
})();
