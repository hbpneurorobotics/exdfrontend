(function() {
  'use strict';

  angular
    .module('environmentServiceMock', [])
    .service('environmentService', function() {
      this.isDevMode = jasmine.createSpy('isDevMode');
      this.isPrivateExperiment = jasmine.createSpy('isPrivateExperiment');
    });
})();
