(function() {
  'use strict';

  angular.module('kgInterfaceServiceMock', []).service('kgInterfaceService', [
    '$q',
    function($q) {
      const simulation = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/core/simulation/v1.0.0/9d4ea936-1426-4597-a9d6-9a3d669ae557',
        'nxv:rev': 1
      };

      const artifact = {
        '@id':
          'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/artifacts/muscleforces/v1.0.0/25799a86-6ddf-43ce-9b9b-1ebf75c629a1',
        'nxv:rev': 1
      };

      this.postKgSimulation = jasmine
        .createSpy('postKgSimulation')
        .and.returnValue($q.resolve(simulation));

      this.updateKgSimulation = jasmine
        .createSpy('updateKgSimulation')
        .and.returnValue($q.resolve(simulation));

      this.postKgArtifact = jasmine
        .createSpy('postKgArtifact')
        .and.returnValue($q.resolve(artifact));

      this.putKgArtifactAttachment = jasmine
        .createSpy('putKgArtifactAttachment')
        .and.returnValue($q.resolve(artifact));

      this.updateKgArtifact = jasmine
        .createSpy('updateKgArtifact')
        .and.returnValue($q.resolve(artifact));
    }
  ]);
})();
