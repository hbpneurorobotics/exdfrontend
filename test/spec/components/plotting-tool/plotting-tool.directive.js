'use strict';

// eslint-disable-next-line no-unused-vars
let Plotly = {
  plot: function() {},
  react: function() {},
  relayout: function() {}
};

describe('Directive: plotting-tool', function() {
  var $scope;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('simulationInfoMock'));
  beforeEach(module('stateServiceMock'));
  beforeEach(module('dynamicViewOverlayServiceMock'));
  beforeEach(module('goldenLayoutServiceMock'));
  beforeEach(module('currentStateMockFactory'));
  beforeEach(module('roslibMock'));
  beforeEach(module('sceneInfoMock'));
  beforeEach(module('backendInterfaceServiceMock'));
  beforeEach(module('userInteractionSettingsServiceMock'));

  beforeEach(
    module(function($provide) {
      $provide.value('panels', {
        close: jasmine.createSpy('close')
      });
    })
  );

  beforeEach(
    inject(function($rootScope, $compile, EDIT_MODE, STATE, TOOL_CONFIGS) {
      $scope = $rootScope.$new();
      $scope.EDIT_MODE = EDIT_MODE;
      $scope.STATE = STATE;
      $scope.TOOL_CONFIGS = TOOL_CONFIGS;

      spyOn(document, 'addEventListener');
      $compile('<plotting-tool />')($scope);
      $scope.$digest();
    })
  );

  it('should initialize scope variables correctly', function() {
    expect($scope.categories).toBeDefined();
  });

  it('should reject a size too small', function() {
    expect($scope.checkSize(10)).not.toBe(10);
  });

  it('should generate unique contextual key', function() {
    let hiearchyMock = { parentElement: null, id: 0, children: [] };

    hiearchyMock.children.push({
      parentElement: hiearchyMock,
      id: 1,
      children: []
    });

    expect(
      $scope.findContextualUniqueKey(hiearchyMock.children[0], 'plot')
    ).toBe('plot-0');
  });

  it('should build plot', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');
    $scope.loadSettings = jasmine.createSpy('loadSettings');
    $scope.saveSettings = jasmine.createSpy('saveSettings');

    $scope.showStructureSetup({
      modelTitle: 'Line',
      thumbnail: 'img/esv/plotting-tool/line_charts.png',
      dimensions: 3,
      multi: true,
      hasAxis: true
    });

    $scope.showPlot();
    expect($scope.buildPlotly).toHaveBeenCalled();
    expect(unregisterWatch).toHaveBeenCalled();
  });

  it('should reset plot', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');
    $scope.loadSettings = jasmine.createSpy('loadSettings');
    $scope.saveSettings = jasmine.createSpy('saveSettings');

    $scope.newPlot();
    expect($scope.plotVisible).toBe(false);
    expect($scope.structureSetupVisible).toBe(false);
  });

  it('should build plot with merged dimensions ', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');
    $scope.loadSettings = jasmine.createSpy('loadSettings');
    $scope.saveSettings = jasmine.createSpy('saveSettings');

    $scope.showStructureSetup({
      modelTitle: 'Pie',
      thumbnail: 'img/esv/plotting-tool/pie_charts.png',
      dimensions: 1,
      multi: true,
      hasAxis: false,
      type: 'pie',
      mergedDimensions: true,
      valuesMustBePositive: true
    });

    $scope.plotStructure.plotElements.push(
      $scope.plotStructure.plotElements[0]
    );

    $scope.showPlot();
    expect($scope.buildPlotly).toHaveBeenCalled();
    expect(unregisterWatch).toHaveBeenCalled();
  });

  it('should build plot with y error ', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');
    $scope.loadSettings = jasmine.createSpy('loadSettings');
    $scope.saveSettings = jasmine.createSpy('saveSettings');

    $scope.showStructureSetup({
      modelTitle: 'Error Bars',
      thumbnail: 'img/esv/plotting-tool/error_bars.png',
      dimensions: 3,
      multi: false,
      hasAxis: true,
      type: 'scatter',
      mode: 'lines',
      lastDimensionIsYError: true
    });

    $scope.showPlot();
    expect($scope.buildPlotly).toHaveBeenCalled();
    expect(unregisterWatch).toHaveBeenCalled();
  });

  it('should parse robot topics', function() {
    $scope.topics = {};

    /* eslint-disable camelcase */

    let mockServiceSuccessResponse = {
      sensor_names: [
        'default::robot::some_link::my_camera',
        'default::robot::twist',
        'default::robot::some_link::float'
      ],
      sensor_types: ['camera'],
      sensor_names_ROS: ['camera_01', 'twist', 'float'],
      rostopic_sensor_urls: [
        'camera_01/camera_image',
        'robot/twist',
        'robot/float'
      ],
      rostopic_actuator_urls: ['actuator_01'],
      actuator_ros_message_type: ['std_msgs/Float32'],
      sensor_ros_message_type: [
        'sensor_msgs/Image',
        'geometry_msgs.msg.Twist',
        'std_msgs/Float32'
      ]
    };

    $scope.parseRobotModelTopics(mockServiceSuccessResponse);

    expect(Object.keys($scope.topics).length).toBe(0);
  });

  function fillPlotStructureWithType(topicType, topic) {
    $scope.topics[topic] = topicType;

    for (var i = 0; i < $scope.plotStructure.plotElements.length; i++) {
      for (
        var di = 0;
        di < $scope.plotStructure.plotElements[i].dimensions.length;
        di++
      ) {
        var dimension = $scope.plotStructure.plotElements[i].dimensions[di];

        dimension.source = topic;
      }
    }
  }

  it('should handle standard topic message', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Line',
      thumbnail: 'img/esv/plotting-tool/line_charts.png',
      dimensions: 3,
      multi: true,
      hasAxis: true
    });

    $scope.showPlot();

    let message = {};

    message.data = 10;

    let rosTopicSubMock = {
      name: '/testtopic'
    };

    fillPlotStructureWithType('std_msgs/Float32', '/testtopic');
    $scope.parseStandardMessages.call(rosTopicSubMock, message);

    expect($scope.needPlotUpdate).toBe(true);
  });

  it('should handle model state messages', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Line',
      thumbnail: 'img/esv/plotting-tool/line_charts.png',
      dimensions: 3,
      multi: true,
      hasAxis: true
    });

    $scope.showPlot();

    let message = {};

    message.name = ['husky'];
    message.pose = [
      {
        position: { x: 0, y: 1, z: 2 },
        orientation: { x: 0, y: 1, z: 2, w: 1 }
      }
    ];

    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.x'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.y'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.z'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/angle.x'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/angle.y'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/angle.z'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);

    expect($scope.needPlotUpdate).toBe(true);
  });

  it('should handle model state message with pie', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Pie',
      thumbnail: 'img/esv/plotting-tool/pie_charts.png',
      dimensions: 1,
      multi: true,
      hasAxis: false,
      type: 'pie',
      mergedDimensions: true,
      valuesMustBePositive: true
    });

    $scope.showPlot();

    let message = {};

    message.name = ['husky'];
    message.pose = [
      {
        position: { x: 0, y: 1, z: 2 },
        orientation: { x: 0, y: 1, z: 2, w: 1 }
      }
    ];

    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.x'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.y'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.z'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);

    expect($scope.needPlotUpdate).toBe(true);
  });

  it('should handle oodel bar m', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Bar',
      thumbnail: 'img/esv/plotting-tool/bar_charts.png',
      dimensions: 1,
      multi: true,
      hasAxis: false,
      type: 'bar',
      mergedDimensions: true,
      mergedDimensionsUseXY: true
    });

    $scope.showPlot();

    let message = {};

    message.name = ['husky'];
    message.pose = [
      {
        position: { x: 0, y: 1, z: 2 },
        orientation: { x: 0, y: 1, z: 2, w: 1 }
      }
    ];

    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.x'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.y'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.z'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);

    expect($scope.needPlotUpdate).toBe(true);
  });

  it('should handle model state message with y error', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Error Bars',
      thumbnail: 'img/esv/plotting-tool/error_bars.png',
      dimensions: 3,
      multi: false,
      hasAxis: true,
      type: 'scatter',
      mode: 'lines',
      lastDimensionIsYError: true
    });

    $scope.showPlot();

    let message = {};

    message.name = ['husky'];
    message.pose = [
      {
        position: { x: 0, y: 1, z: 2 },
        orientation: { x: 0, y: 1, z: 2, w: 1 }
      }
    ];

    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.x'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.y'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.z'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);

    expect($scope.needPlotUpdate).toBe(true);
  });

  it('should handle model state message with marker size', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Bubble',
      thumbnail: 'img/esv/plotting-tool/bubble_chart.png',
      dimensions: 3,
      multi: false,
      hasAxis: true,
      type: 'scatter',
      mode: 'marker',
      lastDimensionIsSize: true
    });

    $scope.showPlot();

    let message = {};

    message.name = ['husky'];
    message.pose = [
      {
        position: { x: 0, y: 1, z: 2 },
        orientation: { x: 0, y: 1, z: 2, w: 1 }
      }
    ];

    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.x'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.y'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);
    fillPlotStructureWithType(
      'gazebo_msgs/ModelStates',
      '/husky/model_state/position.z'
    );
    $scope.modelStateLastTime = undefined;
    $scope.parseModelStateMessages(message);

    expect($scope.needPlotUpdate).toBe(true);
  });

  it('should update plotly', function() {
    let unregisterWatch = jasmine.createSpy('unregisterWatch');
    $scope.unregisterWatch = unregisterWatch;
    $scope.buildPlotly = jasmine.createSpy('buildPlotly');

    $scope.showStructureSetup({
      modelTitle: 'Error Bars',
      thumbnail: 'img/esv/plotting-tool/error_bars.png',
      dimensions: 3,
      multi: false,
      hasAxis: true,
      type: 'scatter',
      mode: 'lines',
      lastDimensionIsYError: true
    });

    $scope.showPlot();

    $scope.updatePlotly = jasmine.createSpy('updatePlotly');

    $scope.needPlotUpdate = true;
    $scope.onTimeout();
    expect($scope.needPlotUpdate).toBe(false);
  });

  it('should add topic', function() {
    $scope.supportedTypes.push('geometry_msgs.msg.Twist');
    $scope.topics = [];
    $scope.addTopic('topictest', 'geometry_msgs.msg.Twist');
    $scope.addTopic('topictest2', 'std_msgs/Float32');
    expect(Object.keys($scope.topics).length).toBe(7);
  });

  it('should update visible charts when toggling category', function() {
    spyOn($scope, 'updateVisibleModels');

    let categoryMock = { visible: true };

    $scope.toggleVisibleCategory(categoryMock);

    expect($scope.updateVisibleModels).toHaveBeenCalled();
    expect($scope.isCategoryVisible(categoryMock)).toBe(false);
  });
});
