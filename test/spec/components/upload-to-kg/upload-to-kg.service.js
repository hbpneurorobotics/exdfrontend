(function() {
  'use strict';

  describe('Service: uploadToKgService', function() {
    let uploadToKgService, kgInterfaceService, $rootScope;
    let $q;

    const MOCKED_DATA = {
      uploadedArtifacts: [
        {
          idNumber: 'qek3o-pofq2-ijq2',
          identifier: 'artifact1',
          revision: 1,
          uploadedDate: '20-03-2019'
        },
        {
          idNumber: 'i2jio-pmjd2-mmw2d',
          identifier: 'artifact2',
          revision: 1
        }
      ]
    };

    beforeEach(module('uploadToKg'));
    beforeEach(module('kgInterfaceServiceMock'));
    beforeEach(module('storageServerMock'));
    beforeEach(
      inject(function(
        _$rootScope_,
        _$q_,
        _uploadToKgService_,
        _kgInterfaceService_
      ) {
        $rootScope = _$rootScope_;
        $q = _$q_;
        uploadToKgService = _uploadToKgService_;
        kgInterfaceService = _kgInterfaceService_;
      })
    );

    it('should post a simulation instance', function() {
      let simulation = uploadToKgService.uploadSimulation('body');
      $rootScope.$digest();

      expect(kgInterfaceService.postKgSimulation).toHaveBeenCalled();

      simulation = uploadToKgService.uploadSimulation('body');
      $rootScope.$digest();

      expect(kgInterfaceService.updateKgSimulation).toHaveBeenCalled();
      expect(simulation).toEqual(
        $q.resolve({
          '@id':
            'https://nexus-int.humanbrainproject.org/v0/data/sp10/core/simulation/v1.0.0/9d4ea936-1426-4597-a9d6-9a3d669ae557',
          revision: 1
        })
      );
    });

    it('should post an artifact instance', function() {
      let artifact = uploadToKgService.uploadArtifact(
        'type',
        'identifier',
        'body'
      );
      $rootScope.$digest();

      expect(kgInterfaceService.postKgArtifact).toHaveBeenCalled();

      artifact = uploadToKgService.uploadArtifact('type', 'identifier', 'body');
      $rootScope.$digest();

      expect(kgInterfaceService.updateKgArtifact).toHaveBeenCalled();
      expect(artifact).toEqual(
        $q.resolve({
          '@id':
            'https://nexus-int.humanbrainproject.org/v0/data/sp10editor/artifacts/muscleforces/v1.0.0/25799a86-6ddf-43ce-9b9b-1ebf75c629a1',
          'nxv:rev': 1
        })
      );
    });

    it('should upload an attachment', function() {
      uploadToKgService.uploadedArtifacts = MOCKED_DATA.uploadedArtifacts;

      uploadToKgService.uploadAttachment('type', 'qek3o-pofq2-ijq2', 'body');
      $rootScope.$digest();

      expect(uploadToKgService.uploadedArtifacts[0].uploadedDate).not.toBe('');
    });

    it('should fail if attachment does not refer to an uploaded artifact', function() {
      uploadToKgService.uploadAttachment('type', 'qek3o-pofq2-ijq2', 'body');

      let res = uploadToKgService.uploadAttachment(
        'type',
        'qek3o-pofq2-ijq2',
        'body'
      );
      $rootScope.$digest();

      expect(res).toEqual($q.reject());
    });

    it('should return the latest uploading date', function() {
      uploadToKgService.uploadedArtifacts = MOCKED_DATA.uploadedArtifacts;
      $rootScope.$digest();

      expect(uploadToKgService.getUploadedDate('artifact1')).not.toBe('');
    });
  });
})();
