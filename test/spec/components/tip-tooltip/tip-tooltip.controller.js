'use strict';

describe('Controller: tip-tooltip controller', function() {
  let $controller,
    tipTooltipController,
    tipTooltipService,
    controllerScope,
    controllerTipClusters,
    controllerTipCodes,
    DEFAULT_TIP_CLUSTER_IDENTIFIER;

  beforeEach(() => {
    module('tipTooltipModule');
    module('exdFrontendApp');
  });

  beforeEach(
    inject(function(
      _$controller_,
      _$rootScope_,
      _tipTooltipService_,
      _DEFAULT_TIP_CLUSTER_IDENTIFIER_
    ) {
      // Prepare constants.
      DEFAULT_TIP_CLUSTER_IDENTIFIER = _DEFAULT_TIP_CLUSTER_IDENTIFIER_;

      // Prepare the tip tooltip service.
      localStorage.clear();
      tipTooltipService = _tipTooltipService_;
      tipTooltipService.reset();

      // Prepare the controller.
      $controller = _$controller_;
      // Set the scope to dummy values.
      let $scope = _$rootScope_.$new();
      $scope.clusterIdentifier = 'general';
      $scope.enableHideAll = 'true';
      // Initialize the controller.
      tipTooltipController = $controller('TipTooltipController', {
        $scope: $scope
      });

      // Prepare some local variables for easy usage.
      controllerScope = tipTooltipController.$scope;
      controllerTipClusters = controllerScope.tipClusters;
      controllerTipCodes = controllerScope.tipCodes;
    })
  );

  it('should handle first visible', function() {
    controllerTipCodes.WELCOME.hidden = true;
    expect(
      tipTooltipController.isFirstDisplayedInThisTipCluster(
        controllerTipCodes.WELCOME
      )
    ).toBe(false);
    controllerTipCodes.WELCOME.hidden = false;

    tipTooltipService.setCurrentTip(controllerTipCodes.WELCOME);
    tipTooltipService.setCurrentTip(controllerTipCodes.NAVIGATION);

    expect(
      tipTooltipController.isFirstDisplayedInThisTipCluster(
        controllerTipCodes.WELCOME
      )
    ).toBe(false);
    expect(
      tipTooltipController.isFirstDisplayedInThisTipCluster(
        controllerTipCodes.NAVIGATION
      )
    ).toBe(true);
  });

  it('should hide a tip', function() {
    tipTooltipController.hideTipAndMarkAsRead(controllerTipCodes.WELCOME);
    expect(controllerTipCodes.WELCOME.hidden).toBe(true);
  });

  it('should hide an active tip', function() {
    tipTooltipService.setCurrentTip(controllerTipCodes.WELCOME);
    expect(
      controllerTipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);
    tipTooltipController.hideTipAndMarkAsRead(controllerTipCodes.WELCOME);
    expect(controllerTipCodes.WELCOME.hidden).toBe(true);
    expect(
      controllerTipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);
  });

  it('should correctly hide tips and mark them as read without removing them', function() {
    tipTooltipService.setCurrentTip(controllerTipCodes.WELCOME);

    expect(
      controllerTipCodes.WELCOME.keepHiddenPropertyOnReset === undefined ||
        controllerTipCodes.WELCOME.keepHiddenPropertyOnReset === false
    ).toBe(true);
    expect(controllerTipCodes.WELCOME.hidden).toBe(false);
    expect(controllerTipCodes.WELCOME.markedAsRead).toBe(false);
    expect(
      controllerTipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);

    tipTooltipController.hideTipAndMarkAsRead(controllerTipCodes.WELCOME);

    expect(controllerTipCodes.WELCOME.hidden).toBe(true);
    expect(controllerTipCodes.WELCOME.markedAsRead).toBe(true);
    expect(
      controllerTipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);
  });

  it('should correctly hide tips and mark them as read', function() {
    tipTooltipService.setCurrentTip(controllerTipCodes.OBJECT_LIBRARY_INFO);

    expect(controllerTipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(false);
    expect(controllerTipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(false);
    expect(
      controllerTipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);

    tipTooltipController.hideTipAndMarkAsRead(
      controllerTipCodes.OBJECT_LIBRARY_INFO
    );

    expect(controllerTipCodes.OBJECT_LIBRARY_INFO.hidden).toBe(true);
    expect(controllerTipCodes.OBJECT_LIBRARY_INFO.markedAsRead).toBe(true);
    expect(
      controllerTipClusters[DEFAULT_TIP_CLUSTER_IDENTIFIER].tips.length
    ).toBe(1);
  });

  it('should store the markedAsRead flag in the local storage', function() {
    spyOn(localStorage, 'setItem');

    tipTooltipService.setCurrentTip(controllerTipCodes.WELCOME);

    tipTooltipController.hideTipAndMarkAsRead(controllerTipCodes.WELCOME);

    expect(localStorage.setItem).toHaveBeenCalled();
  });

  it('should move to previous/next tips', function() {
    controllerTipCodes.SIMULATIONS_TIPS.tipListPos = null;
    tipTooltipService.setCurrentTip(controllerTipCodes.SIMULATIONS_TIPS);
    tipTooltipController.showNextCard(controllerTipCodes.SIMULATIONS_TIPS);
    expect(controllerTipCodes.SIMULATIONS_TIPS.tipListPos).toBe(1);
    tipTooltipController.showPreviousCard(controllerTipCodes.SIMULATIONS_TIPS);
    expect(controllerTipCodes.SIMULATIONS_TIPS.tipListPos).toBe(0);
  });

  it('should correctly indicate when to show the hide all button', function() {
    expect(
      tipTooltipController.displayHideAllTipsButton(controllerTipCodes.WELCOME)
    ).toBe(false);

    tipTooltipService.setCurrentTip(controllerTipCodes.WELCOME);

    expect(
      tipTooltipController.displayHideAllTipsButton(controllerTipCodes.WELCOME)
    ).toBe(true);

    tipTooltipService.setCurrentTip(controllerTipCodes.NAVIGATION);

    expect(
      tipTooltipController.displayHideAllTipsButton(controllerTipCodes.WELCOME)
    ).toBe(false);
    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.NAVIGATION
      )
    ).toBe(true);

    tipTooltipService.setCurrentTip(controllerTipCodes.SIMULATIONS_TIPS);

    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.SIMULATIONS_TIPS
      )
    ).toBe(true);
    expect(
      tipTooltipController.displayHideAllTipsButton(controllerTipCodes.WELCOME)
    ).toBe(false);

    tipTooltipController.hideTipAndMarkAsRead(controllerTipCodes.NAVIGATION);

    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.SIMULATIONS_TIPS
      )
    ).toBe(true);

    tipTooltipController.hideTipAndMarkAsRead(controllerTipCodes.WELCOME);

    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.SIMULATIONS_TIPS
      )
    ).toBe(true);

    tipTooltipService.resetAllTipCodeTipPreferences();

    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.SIMULATIONS_TIPS
      )
    ).toBe(true);

    let previousValue = controllerScope.enableHideAll;
    controllerScope.enableHideAll = false;

    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.SIMULATIONS_TIPS
      )
    ).toBe(false);
    expect(
      tipTooltipController.displayHideAllTipsButton(
        controllerTipCodes.NAVIGATION
      )
    ).toBe(false);
    expect(
      tipTooltipController.displayHideAllTipsButton(controllerTipCodes.WELCOME)
    ).toBe(false);

    controllerScope.enableHideAll = previousValue;
  });

  it('should be able to hide all', function() {
    tipTooltipService.setCurrentTip(controllerTipCodes.WELCOME);
    tipTooltipService.setCurrentTip(controllerTipCodes.NAVIGATION, 'test');
    tipTooltipService.setCurrentTip(
      controllerTipCodes.SIMULATIONS_TIPS,
      'test'
    );

    expect(controllerTipCodes.WELCOME.hidden).toBe(false);
    expect(controllerTipCodes.NAVIGATION.hidden).toBe(false);
    expect(controllerTipCodes.SIMULATIONS_TIPS.hidden).toBe(false);

    tipTooltipController.hideAllTips();

    expect(controllerTipCodes.WELCOME.hidden).toBe(true);
    expect(controllerTipCodes.NAVIGATION.hidden).toBe(true);
    expect(controllerTipCodes.SIMULATIONS_TIPS.hidden).toBe(true);
  });
});
