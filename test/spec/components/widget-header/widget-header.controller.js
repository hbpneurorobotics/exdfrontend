'use strict';

describe('Controller: widget-header controller', function() {
  let $controller, widgetHeaderController, tipTooltipService;

  beforeEach(() => {
    module('exdFrontendApp');
    module('widgetHeaderModule');
    module('tipTooltipServiceMock');
  });

  beforeEach(
    inject(function(_$controller_, _$rootScope_, _tipTooltipService_) {
      $controller = _$controller_;
      tipTooltipService = _tipTooltipService_;

      let $scope = _$rootScope_.$new();
      $scope.infoTipCode = 'OBJECT_LIBRARY_INFO';

      widgetHeaderController = $controller('WidgetHeaderController', {
        $scope: $scope,
        tipTooltipService: tipTooltipService
      });
    })
  );

  it('should show tip', function() {
    tipTooltipService.tipCodes['OBJECT_LIBRARY_INFO'].hidden = true;
    widgetHeaderController.showWidgetHint();
    expect(tipTooltipService.setCurrentTip).toHaveBeenCalled();
  });

  it('should hide tip', function() {
    tipTooltipService.tipCodes['OBJECT_LIBRARY_INFO'].hidden = false;
    widgetHeaderController.showWidgetHint();
    expect(tipTooltipService.tipCodes['OBJECT_LIBRARY_INFO'].hidden).toBe(true);
  });
});
