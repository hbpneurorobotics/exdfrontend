'use strict';

describe('Controller: ApplicationTopToolbarController', function() {
  let applicationTopToolbarController;

  let $controller, $rootScope, $scope, $window;
  let applicationTopToolbarService,
    bbpConfig,
    environmentRenderingService,
    experimentViewService,
    nrpAnalytics,
    simToolsSidebarService,
    simulationInfo,
    userContextService,
    recorderPanelService,
    authService;

  beforeEach(module('exdFrontendApp'));

  // used outside simulation
  beforeEach(module('applicationTopToolbarServiceMock'));
  beforeEach(module('nrpAnalyticsMock'));
  beforeEach(module('storageServerMock'));
  beforeEach(module('userContextServiceMock'));
  // used inside simulation
  beforeEach(module('environmentRenderingServiceMock'));
  beforeEach(module('experimentViewServiceMock'));
  beforeEach(module('simToolsSidebarServiceMock'));
  beforeEach(module('simulationInfoMock'));
  beforeEach(module('stateServiceMock'));
  beforeEach(module('nrpUserMock'));
  beforeEach(module('authServiceMock'));

  beforeEach(
    inject(function(
      _$controller_,
      _$rootScope_,
      _$window_,
      _applicationTopToolbarService_,
      _bbpConfig_,
      _environmentRenderingService_,
      _experimentViewService_,
      _nrpAnalytics_,
      _simToolsSidebarService_,
      _simulationInfo_,
      _userContextService_,
      _recorderPanelService_,
      _authService_
    ) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      $window = _$window_;
      applicationTopToolbarService = _applicationTopToolbarService_;
      bbpConfig = _bbpConfig_;
      environmentRenderingService = _environmentRenderingService_;
      experimentViewService = _experimentViewService_;
      nrpAnalytics = _nrpAnalytics_;
      simToolsSidebarService = _simToolsSidebarService_;
      simulationInfo = _simulationInfo_;
      userContextService = _userContextService_;
      recorderPanelService = _recorderPanelService_;
      authService = _authService_;
    })
  );

  beforeEach(function() {
    applicationTopToolbarService.isInSimulationView.and.returnValue(true);

    $scope = $rootScope.$new();
    applicationTopToolbarController = $controller(
      'ApplicationTopToolbarController',
      {
        $rootScope: $rootScope,
        $scope: $scope
      }
    );
  });

  it(' - openMenu()', function() {
    let mockMdMenu = {
      open: jasmine.createSpy('open')
    };
    let mockEvent = {};

    applicationTopToolbarController.openMenu(mockMdMenu, mockEvent);
    expect(mockMdMenu.open).toHaveBeenCalledWith(mockEvent);
  });

  it(' - onButtonLogout() in list experiments view', function() {
    spyOn(
      applicationTopToolbarController,
      'isExperimentRunning'
    ).and.returnValue(false);
    applicationTopToolbarController.onButtonLogout();

    expect(authService.logout).toHaveBeenCalled();
    expect($window.location.reload).toHaveBeenCalled();
  });

  it(' - onButtonLogout() when experiment is running', function() {
    spyOn(
      applicationTopToolbarController,
      'isExperimentRunning'
    ).and.returnValue(true);
    applicationTopToolbarController.onButtonLogout();
    expect(
      experimentViewService.openExitDialogWhileChangingUsers
    ).toHaveBeenCalled();
  });

  it(' - onButtonSetSimulationState()', function() {
    let mockState = {};
    applicationTopToolbarController.onButtonSetSimulationState(mockState);

    expect(experimentViewService.setSimulationState).toHaveBeenCalledWith(
      mockState
    );
  });

  it(' - onButtonReset()', function() {
    applicationTopToolbarController.onButtonReset();

    expect(experimentViewService.resetSimulation).toHaveBeenCalled();
  });

  it(' - onButtonExit()', function() {
    // demo mode = false
    applicationTopToolbarController.onButtonExit();
    expect(experimentViewService.openExitDialog).toHaveBeenCalled();
    expect(experimentViewService.exitSimulation).not.toHaveBeenCalled();

    // demo mode = true
    spyOn(bbpConfig, 'get').and.callFake((value, defaultValue) => {
      if (value === 'demomode.demoCarousel') {
        return true;
      } else {
        return defaultValue;
      }
    });
    applicationTopToolbarController.onButtonExit();
    expect(experimentViewService.exitDemo).toHaveBeenCalled();
  });

  it(' - allowPlayPause()', function() {
    userContextService.isOwner.and.returnValue(false);
    expect(applicationTopToolbarController.allowPlayPause()).toBe(false);

    userContextService.isOwner.and.returnValue(true);
    expect(applicationTopToolbarController.allowPlayPause()).toBe(true);
  });

  it('onButtonUpload()', function() {
    applicationTopToolbarController.onButtonUpload();

    expect(experimentViewService.openUploadDialog).toHaveBeenCalled();
  });

  it('onButtonRecord()', function() {
    spyOn(recorderPanelService, 'toggleShow');
    applicationTopToolbarController.onButtonRecord();
    expect(recorderPanelService.toggleShow).toHaveBeenCalled();
  });

  describe('outside a running experiment/simulation view', function() {
    beforeEach(function() {
      experimentViewService.isInSimulationView.and.returnValue(false);
    });

    beforeEach(function() {
      $scope = $rootScope.$new();
      applicationTopToolbarController = $controller(
        'ApplicationTopToolbarController',
        {
          $rootScope: $rootScope,
          $scope: $scope
        }
      );
    });
  });

  describe('inside a running experiment/simulation view', function() {
    beforeEach(function() {
      experimentViewService.isInSimulationView.and.returnValue(true);
      simulationInfo.experimentDetails.name = 'test experiment 123';
    });

    beforeEach(function() {
      $scope = $rootScope.$new();
      applicationTopToolbarController = $controller(
        'ApplicationTopToolbarController',
        {
          $rootScope: $rootScope,
          $scope: $scope
        }
      );
    });

    it(' - constructor()', function() {
      expect(
        applicationTopToolbarController.environmentRenderingService
      ).toBeDefined();
      expect(
        applicationTopToolbarController.simToolsSidebarService
      ).toBeDefined();
      expect(applicationTopToolbarController.simulationInfo).toBeDefined();
      expect(applicationTopToolbarController.stateService).toBeDefined();
    });

    it(' - onButtonEnvironmentSettings()', function() {
      environmentRenderingService.loadingEnvironmentSettingsPanel = false;
      applicationTopToolbarController.onButtonEnvironmentSettings();
      expect(nrpAnalytics.eventTrack).toHaveBeenCalled();
    });

    it(' - toggleSimulationToolsSidebar()', function() {
      applicationTopToolbarController.toggleSimulationToolsSidebar();
      expect(simToolsSidebarService.toggleSidebar).toHaveBeenCalled();
    });

    it(' - nrpUserDisplay', function() {
      $scope.$digest();
      expect(applicationTopToolbarController.currentUser).toEqual('mockUser');
    });
  });
});
