'use strict';

describe('Services: modelsLibraries', function() {
  var modelsLibrariesService, $q, $rootScope, storageServer;
  beforeEach(module('exdFrontendApp'));
  beforeEach(module('modelsLibraries'));
  beforeEach(module('storageServerMock'));

  beforeEach(
    inject(function(
      _nrpUser_,
      _modelsLibrariesService_,
      _$q_,
      _$rootScope_,
      _storageServer_
    ) {
      modelsLibrariesService = _modelsLibrariesService_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      storageServer = _storageServer_;
    })
  );

  it('should generate all the robot models correctly', () => {
    const templateModels = [
      {
        description:
          'Modified Hollie arm model for force based index finger movements.↵      In contrast to the first Hollie arm model it was required to remove the↵      PID control of the index finger joints to allow force control for this↵      particular finger.',
        id: 'arm_robot_force',
        maturity: 'production',
        name: 'Arm robot force based version',
        path: 'arm_robot_force / model.config',
        sdf: 'arm_robot_force.sdf',
        thumbnail: 'thumbnail.png'
      },
      {
        description:
          'Model of an industrial arm and hand robot.↵        The arm: Schunk Powerball Lightweight Arm LWA 4P↵        The hand: Schunk SVH 5-finger hand.',
        id: 'arm_robot',
        maturity: 'development',
        name: 'Arm robot',
        path: 'arm_robot/model.config',
        sdf: 'arm_robot.sdf',
        thumbnail: 'thumbnail.png'
      }
    ];

    storageServer.getAllModels.and.returnValue($q.resolve(templateModels));

    modelsLibrariesService.generateModels('robots').then(res => {
      expect(res[0].sdf).toBe(templateModels[0].sdf);
      expect(storageServer.getAllModels).toHaveBeenCalledWith('robots');
    });
    $rootScope.$apply();
  });

  it('should generate all the brain models correctly', () => {
    const templateModels = [
      {
        modelTitle: 'a brain',
        thumbnail: 'thumbnail.png',
        isShared: false,
        isBrain: true,
        script: 'script',
        description: 'brain description'
      },
      {
        modelTitle: 'a brain',
        thumbnail: 'thumbnail.png',
        isShared: false,
        isBrain: true,
        script: 'script',
        description: 'brain description'
      }
    ];

    const kgModels = [
      {
        modelTitle: 'a kg brain',
        thumbnail: 'thumbnail.png',
        isShared: false,
        isBrain: true,
        script: 'script',
        description: 'brain description',
        urls: {
          fileUrl: 'http://www.url.com'
        }
      },
      {
        modelTitle: 'a kg brain',
        thumbnail: 'thumbnail.png',
        isShared: false,
        isBrain: true,
        script: 'script',
        description: 'brain description',
        urls: {
          fileUrl: 'http://www.url.com'
        }
      }
    ];

    storageServer.getAllModels.and.returnValue($q.resolve(templateModels));
    storageServer.getKnowledgeGraphBrains = jasmine
      .createSpy()
      .and.returnValue($q.resolve(kgModels));

    modelsLibrariesService.generateModels('brains').then(res => {
      expect(res[0].modelTitle).toBe(templateModels[0].modelTitle);
      expect(res[2].isKnowledgeGraphBrain).toBe(true);
      expect(storageServer.getAllModels).toHaveBeenCalledWith('brains');
      expect(storageServer.getKnowledgeGraphBrains).toHaveBeenCalled();
    });
    $rootScope.$apply();
  });

  it('should generate all the models correctly', () => {
    modelsLibrariesService.getModels('robots').then(res => {
      expect(res[0].path).toBe('robot1_name');
      expect(storageServer.getAllModels).toHaveBeenCalledWith('robots');
    });
    $rootScope.$apply();
  });

  it('should set a custom model correctly', () => {
    modelsLibrariesService
      .setCustomModel('p3dx.zip', 'robots', 'fakeContent')
      .then(() =>
        expect(storageServer.setCustomModel).toHaveBeenCalledWith(
          'robots',
          'p3dx.zip',
          'fakeContent'
        )
      );
    $rootScope.$apply();
  });

  it('should delete a custom model correctly', () => {
    modelsLibrariesService
      .deleteCustomModel('fakeModelType', 'fakeFileName')
      // uri gets decoded
      .then(() =>
        expect(storageServer.deleteCustomModel).toHaveBeenCalledWith(
          'fakeModelType',
          'fakeFileName'
        )
      );
    $rootScope.$apply();
  });

  it('should get the config of a robot correctly', () => {
    expect(
      modelsLibrariesService.getRobotConfig({ id: 'p3dx', name: 'p3dx' })
    ).toBe('http://proxy/storage/models/robots/p3dx/config');
  });

  it('should retrieve all models by type', function() {
    var customModels = [
      {
        path: 'fileName',
        name: 'name',
        userId: 'token',
        type: 'robots'
      }
    ];

    modelsLibrariesService.getModelsByType().then(function(res) {
      expect(res[0].toJSON()).toEqual(customModels[0]);
    });
  });
});
