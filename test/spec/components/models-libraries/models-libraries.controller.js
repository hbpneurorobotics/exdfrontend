'use strict';

describe('Controller: ModelsLibrariesController', function() {
  let modelsLibrariesController, modelsLibrariesService;

  let $controller,
    $rootScope,
    $scope,
    $q,
    $httpBackend,
    nrpConfirm,
    nrpErrorDialog;
  const templateModels = [
    {
      description:
        'Modified Hollie arm model for force based index finger movements.↵      In contrast to the first Hollie arm model it was required to remove the↵      PID control of the index finger joints to allow force control for this↵      particular finger.',
      id: 'arm_robot_force',
      maturity: 'production',
      name: 'Arm robot force based version',
      path: 'arm_robot_force / model.config',
      sdf: 'arm_robot_force.sdf',
      thumbnail: 'thumbnail.png'
    },
    {
      description:
        'Model of an idustrial arm and hand robot.↵        The arm: Schunk Powerball Lightweight Arm LWA 4P↵        The hand: Schunk SVH 5-finger hand.',
      id: 'arm_robot',
      maturity: 'development',
      name: 'Arm robot',
      path: 'arm_robot/model.config',
      sdf: 'arm_robot.sdf',
      thumbnail: 'thumbnail.png'
    }
  ];

  const customModels = [
    {
      configPath: 'p3dxbenchmark/model.config',
      description: 'A ROS/Gazebo Pioneer 3DX model.',
      fileName: 'robots/p3dxbenchmark_world3.zip',
      id: 'p3dx',
      name: 'Pioneer 3DX',
      path: 'robots%2Fp3dx.zip',
      sdf: 'p3dx.sdf',
      thumbnail: 'thumbnail.png'
    }
  ];

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('storageServerMock'));
  beforeEach(module('nrpUserMock'));
  beforeEach(module('modelsLibraries'));

  beforeEach(
    inject(function(
      _$controller_,
      _$rootScope_,
      _modelsLibrariesService_,
      _$q_,
      _$httpBackend_,
      _nrpConfirm_,
      _nrpErrorDialog_
    ) {
      $controller = _$controller_;
      $rootScope = _$rootScope_;
      modelsLibrariesService = _modelsLibrariesService_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      nrpConfirm = _nrpConfirm_;
      nrpErrorDialog = _nrpErrorDialog_;
    })
  );

  beforeEach(
    inject(function($compile) {
      $httpBackend.whenGET('http://proxy/models/robots').respond({});
      $httpBackend.whenGET('http://proxy/models/environments').respond({});
      $httpBackend.whenGET('http://proxy/models/brains').respond({});

      $scope = $rootScope.$new();
      modelsLibrariesController = $controller('modelsLibrariesController', {
        $rootScope: $rootScope,
        $scope: $scope
      });
      $compile('<models-libraries />')($scope);
    })
  );

  it('should loadAllModels successfully', function() {
    $rootScope.$apply();
    spyOn(modelsLibrariesService, 'generateModels').and.returnValue(
      $q.resolve([...templateModels, ...customModels])
    );
    spyOn(modelsLibrariesService, 'getRobotConfig').and.returnValue(
      'http://proxy/models/robots/p3dx/config'
    );
    modelsLibrariesController.loadAllModels().then(() => {
      expect(modelsLibrariesService.generateModels).toHaveBeenCalledTimes(3);
      expect(modelsLibrariesController.models[0].name).toBe('robots');
      expect(modelsLibrariesController.models[0].data[0].name).toBe(
        'Arm robot force based version'
      );
      expect(modelsLibrariesController.models[0].data[0].configpath).toBe(
        'http://proxy/models/robots/p3dx/config'
      );
      expect(modelsLibrariesController.models[1].name).toBe('environments');
      expect(modelsLibrariesController.models[1].data[1].maturity).toBe(
        'development'
      );
      expect(modelsLibrariesController.models[2].name).toBe('brains');
      expect(modelsLibrariesController.models[2].data[2].thumbnail).toBe(
        'thumbnail.png'
      );
    });
    $rootScope.$apply();
  });

  it('should find a category successfully', function() {
    expect(modelsLibrariesController.findCategory('robots')).toEqual({
      name: 'robots',
      visible: false,
      data: undefined,
      loading: false
    });
  });

  it('should toggle the visibilty of a category successfully', function() {
    modelsLibrariesController.toggleVisibility('robots');
    expect(
      modelsLibrariesController.findCategory('robots').visible
    ).toBeTruthy();
    modelsLibrariesController.toggleVisibility('robots');
    expect(
      modelsLibrariesController.findCategory('robots').visible
    ).toBeFalsy();
  });

  it('should select an entity', function() {
    $rootScope.$apply();
    spyOn(modelsLibrariesService, 'generateModels').and.returnValue(
      $q.resolve([...templateModels, ...customModels])
    );
    spyOn(modelsLibrariesService, 'getRobotConfig').and.returnValue(
      'http://proxy/models/robots/p3dx/config'
    );
    modelsLibrariesController.loadAllModels().then(() => {
      modelsLibrariesController.selectEntity('robots%2Fp3dx.zip');
      expect(
        modelsLibrariesController.findCategory('robots').data[2].isSelected
      ).toBeTruthy();
    });
    $rootScope.$apply();
  });

  it('should delete a model successfully', function() {
    spyOn(modelsLibrariesService, 'deleteCustomModel').and.returnValue(
      $q.resolve()
    );
    spyOn(nrpConfirm, 'open').and.returnValue($q.resolve());
    spyOn(modelsLibrariesController, 'loadAllModels').and.returnValue(
      $q.resolve()
    );
    modelsLibrariesController
      .deleteModel('fakeModelType', 'fakeFileName')
      .then(() => {
        expect(modelsLibrariesService.deleteCustomModel).toHaveBeenCalledWith(
          'fakeModelType',
          'fakeFileName'
        );
        expect(modelsLibrariesController.loadAllModels).toHaveBeenCalled();
      });
    $rootScope.$apply();
  });

  it('should fail to delete a model', function() {
    spyOn(modelsLibrariesService, 'generateModels').and.returnValue(
      $q.resolve([...templateModels, ...customModels])
    );
    spyOn(modelsLibrariesService, 'getRobotConfig').and.returnValue(
      'http://proxy/models/robots/p3dx/config'
    );
    spyOn(modelsLibrariesService, 'deleteCustomModel').and.returnValue(
      $q.reject('Error,could not delete custom model')
    );
    spyOn(nrpErrorDialog, 'open');
    spyOn(nrpConfirm, 'open').and.returnValue($q.resolve());
    modelsLibrariesController
      .deleteModel('fakeModelType', 'fakeFileName')
      .then(() => {
        expect(modelsLibrariesService.deleteCustomModel).toHaveBeenCalledWith(
          'fakeModelType',
          'fakeFileName'
        );
        expect(nrpErrorDialog.open).toHaveBeenCalledWith({
          type: 'Error.',
          message: 'Error,could not delete custom model'
        });
      });
    $rootScope.$apply();
  });

  it('should open an error dialog if the file to upload has not the zip extension', function() {
    spyOn(nrpErrorDialog, 'open');
    modelsLibrariesController.uploadModelZip({ type: 'wrong type' }, {});
    $scope.$digest();
    expect(nrpErrorDialog.open).toHaveBeenCalled();
  });

  it('should upload a custom model when a valid zip is provided and reload the models', function() {
    spyOn(window, 'FileReader').and.returnValue({
      readAsArrayBuffer: function() {
        this.onload({ target: { result: 'fakeZip' } });
      }
    });
    spyOn(modelsLibrariesController, 'loadAllModels').and.returnValue(
      $q.resolve()
    );
    spyOn(modelsLibrariesController, 'selectEntity').and.callFake(
      () => undefined
    );
    spyOn(modelsLibrariesController, 'findCategory').and.returnValue({
      data: [
        {
          path: {
            toString: () => 'fakeZip',
            includes: () => true
          }
        }
      ]
    });
    modelsLibrariesService.setCustomModel = jasmine
      .createSpy()
      .and.returnValue($q.resolve({ data: 'fakeZip' }));
    const entityType = 'entityType';
    modelsLibrariesController.uploadModelZip(
      { type: 'application/zip' },
      entityType
    );
    $rootScope.$digest();
    expect(modelsLibrariesService.setCustomModel).toHaveBeenCalled();
    expect(modelsLibrariesController.loadAllModels).toHaveBeenCalled();
    expect(modelsLibrariesController.uploadingModel).toBe(false);
  });

  it('should createErrorPopupwhen failing to setCustomModel', function() {
    spyOn(window, 'FileReader').and.returnValue({
      readAsArrayBuffer: function() {
        this.onload({ target: { result: 'fakeZip' } });
      }
    });
    spyOn(modelsLibrariesController, 'createErrorPopup').and.returnValue(
      $q.resolve()
    );
    modelsLibrariesService.setCustomModel = jasmine
      .createSpy()
      .and.returnValue($q.reject({}));
    const entityType = 'entityType';
    modelsLibrariesController.uploadModelZip(
      { type: 'application/zip' },
      entityType
    );
    $rootScope.$digest();
    expect(modelsLibrariesController.createErrorPopup).toHaveBeenCalled();
    expect(modelsLibrariesService.setCustomModel).toHaveBeenCalled();
  });

  it('should call uploadModelZip when uploading a model', () => {
    const inputMock = [];
    inputMock.on = jasmine.createSpy();
    inputMock.click = jasmine.createSpy();
    inputMock.push({ files: [{ type: 'application/zip' }] });
    spyOn(window, '$').and.returnValue(inputMock);
    spyOn(document.body, 'appendChild');
    spyOn(document.body, 'removeChild');

    spyOn(modelsLibrariesController, 'uploadModelZip');
    const modelType = 'modelType';
    modelsLibrariesController.uploadModel(modelType);

    expect(window.$).toHaveBeenCalled();
    expect(inputMock.on).toHaveBeenCalledTimes(1);
    expect(inputMock.click).toHaveBeenCalled();
    expect(inputMock.on.calls.count()).toBe(1);
    const uploadCallback = inputMock.on.calls.argsFor(0)[1];
    uploadCallback({ target: { files: [] } });
    expect(modelsLibrariesController.uploadModelZip).toHaveBeenCalled();
    expect(modelsLibrariesController.uploadingModel).toBe(true);
  });

  it('should ask to override the model if it already exists ', function() {
    spyOn(window, 'FileReader').and.returnValue({
      readAsArrayBuffer: function() {
        this.onload({ target: { result: 'fakeZip' } });
      }
    });
    spyOn(modelsLibrariesController, 'loadAllModels').and.returnValue(
      $q.resolve()
    );
    spyOn(modelsLibrariesController, 'selectEntity').and.callFake(
      () => undefined
    );
    spyOn(modelsLibrariesController, 'findCategory').and.returnValue({
      data: [
        {
          path: {
            toString: () => 'fakeZip',
            includes: () => true
          }
        }
      ]
    });
    spyOn(nrpConfirm, 'open').and.returnValue($q.resolve());
    modelsLibrariesService.setCustomModel = jasmine
      .createSpy()
      .and.returnValues(
        window.$q.reject({
          data: 'One of your custom models already has the name'
        }),
        window.$q.resolve({ path: 'filename' })
      );
    const entityType = 'robots';
    modelsLibrariesController.uploadModelZip(
      { type: 'application/zip' },
      entityType
    );
    $rootScope.$digest();
    expect(modelsLibrariesController.loadAllModels).toHaveBeenCalled();
    expect(modelsLibrariesService.setCustomModel).toHaveBeenCalledTimes(2);
    expect(nrpConfirm.open).toHaveBeenCalled();
    expect(modelsLibrariesController.uploadingModel).toBe(false);
  });
});
