'use strict';

describe('Directive: gdpr-prompt', function() {
  beforeEach(module('exdFrontendApp'));
  beforeEach(module('authServiceMock'));

  beforeEach(
    module($provide => {
      $provide.value('nrpModalService', {
        createModal: jasmine.createSpy()
      });

      $provide.value('storageServer', {
        getGdprStatus: jasmine.createSpy(),
        acceptGdpr: jasmine.createSpy()
      });

      $provide.value('$window', {
        location: {
          reload: jasmine.createSpy()
        }
      });
    })
  );

  it(
    'should not show dialod if gdpr has been previously accepted',
    inject((storageServer, $rootScope, $compile, nrpModalService) => {
      storageServer.getGdprStatus.and.returnValue(
        Promise.resolve({ gdpr: true })
      );

      $compile('<gdpr-prompt/>')($rootScope);
      $rootScope.$digest();

      expect(nrpModalService.createModal).not.toHaveBeenCalled();
    })
  );

  it(
    'should clear auth token and reload page if terms of service not accepted',
    inject(
      (
        storageServer,
        $rootScope,
        $compile,
        nrpModalService,
        $window,
        $q,
        authService
      ) => {
        storageServer.getGdprStatus.and.returnValue(
          $q.resolve({ gdpr: false })
        );

        nrpModalService.createModal.and.returnValue($q.resolve(false));
        $window.location.reload.calls.reset();

        $compile('<gdpr-prompt/>')($rootScope);
        $rootScope.$digest();

        expect(storageServer.acceptGdpr).not.toHaveBeenCalled();
        expect($window.location.reload).toHaveBeenCalled();
        expect(authService.logout).toHaveBeenCalled();
      }
    )
  );

  it(
    'should make as gdpr accepted if terms of service accepted',
    inject(
      (
        storageServer,
        $rootScope,
        $compile,
        nrpModalService,
        authService,
        $window,
        $q
      ) => {
        storageServer.getGdprStatus.and.returnValue(
          $q.resolve({ gdpr: false })
        );
        $window.location.reload.calls.reset();
        nrpModalService.createModal.and.returnValue($q.resolve(true));

        $compile('<gdpr-prompt/>')($rootScope);
        $rootScope.$digest();

        expect(storageServer.acceptGdpr).toHaveBeenCalled();
        expect(authService.logout).not.toHaveBeenCalled();
        expect($window.location.reload).not.toHaveBeenCalled();
      }
    )
  );
});
