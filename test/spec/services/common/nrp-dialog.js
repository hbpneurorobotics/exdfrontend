'use strict';

describe('Service: nrpConfirm', function() {
  let $uibModal;
  let nrpConfirm;

  beforeEach(
    module(function($provide) {
      $provide.value('$uibModal', {
        open: function() {
          return { result: window.$q.resolve('folder_name') };
        }
      });
    })
  );
  beforeEach(module('nrp-ui-dialog'));

  beforeEach(
    inject(function(_$uibModal_, _nrpConfirm_) {
      $uibModal = _$uibModal_;
      nrpConfirm = _nrpConfirm_;
    })
  );

  it('should open a dialog, including additional options if given', function() {
    spyOn($uibModal, 'open').and.callThrough();

    nrpConfirm.open();
    expect($uibModal.open).toHaveBeenCalled();

    let options = {
      securityQuestion: {}
    };

    nrpConfirm.open(options);
    expect($uibModal.open.calls.count()).toBe(2);
    expect(
      $uibModal.open.calls.mostRecent().args[0].scope.securityQuestion
    ).toBe(options.securityQuestion);
  });
});

describe('Service: nrpError', function() {
  let nrpError;

  beforeEach(module('nrp-error'));

  beforeEach(
    inject(function(_nrpError_) {
      nrpError = _nrpError_;
    })
  );

  it('should be able to create a base NRP error, including additional options if given', function() {
    let error = nrpError.error();
    expect(error).toBeDefined();
    expect(error.message.length > 0).toBeTruthy();
    expect(error.type.length > 0).toBeTruthy();

    let options = {
      message: 'test-message'
    };

    error = nrpError.error(options);
    expect(error).toBeDefined();
    expect(error.message).toBe(options.message);
    expect(error.type.length > 0).toBeTruthy();

    // Error opject as options
    let errorOption = new Error();
    errorOption.data = {};
    error = nrpError.error(errorOption);
    expect(error).toBeDefined();
    expect(error.message.length > 0).toBeTruthy();
    expect(error.type.length > 0).toBeTruthy();
    expect(error.toString().length > 0).toBeTruthy();
  });

  it('should create a HTTP error based on a response', function() {
    // no response should result in fail
    expect(nrpError.httpError).toThrowError(
      "Cannot read property 'status' of undefined"
    );

    let response = {};
    let error = undefined;

    // undefined status code
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.message).toBeDefined();

    // response is a NrpError
    let nrpErrorResponse = nrpError.error();
    error = nrpError.httpError(nrpErrorResponse);
    expect(error).toBe(nrpErrorResponse);

    // status code 0
    response.status = 0;
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.type).toBeDefined();
    expect(error.message).toBeDefined();

    // status code 404
    response.status = 404;
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.type).toBe('NotFound');
    expect(error.message).toBeDefined();

    // status code 403
    response.status = 403;
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.type).toBe('Forbidden');
    expect(error.message).toBeDefined();

    // status code 502
    response.status = 502;
    response.headers = jasmine
      .createSpy('headers')
      .and.returnValue('text/html');
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.type).toBe('BadGateway');
    expect(error.message).toBeDefined();
    expect(response.headers).toHaveBeenCalled();

    // some data in response
    response.status = 999;
    response.data = {
      error: {
        type: 'type',
        message: 'message'
      }
    };
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.type).toBe(response.data.error.type);
    expect(error.message).toBe(response.data.error.message);

    // supply data with reason
    response.data = {
      reason: 'reason'
    };
    error = nrpError.httpError(response);
    expect(error).toBeDefined();
    expect(error.message).toBe(response.data.reason);
  });
});
