'use strict';
/*   4:7  error  '$q' is assigned a value but never used              no-unused-vars
   5:5  error  'nrpModalService' is defined but never used          no-unused-vars
   7:5  error  'nrpUser' is assigned a value but never used         no-unused-vars
   8:5  error  'nrpErrorDialog' is assigned a value but never used  no-unused-vars
   9:5  error  '$http' is assigned a value but never used           no-unused-vars
  10:5  error  'bbpConfig' is assigned a value but never used       no-unused-vars
  13:5  error  '$httpBackend' is assigned a value but never used    no-unused-vars
  88:9  error  'result' is assigned a value but never used          no-unused-vars
  98:9  error  'result' is assigned a value but never used  */
describe('Services: ExperimentsSharingService', function() {
  let storageServer, expSharingService, $rootScope;

  beforeEach(module('exdFrontendApp'));
  beforeEach(module('storageServerMock'));
  beforeEach(module('nrpModalServiceMock'));
  beforeEach(
    inject(function(
      _storageServer_,
      _nrpErrorDialog_,
      _$rootScope_,
      _experimentSharingService_
    ) {
      $rootScope = _$rootScope_;
      storageServer = _storageServer_;
      expSharingService = _experimentSharingService_;
      expSharingService.storageServer = storageServer;
    })
  );

  it('should update the sharing option for the experiment', function() {
    expSharingService.updateSharedEntityMode('private');
    expect(storageServer.updateSharedExpMode).toHaveBeenCalled();
  });

  it('should not update the sharing option for the experiment', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    expSharingService.updateSharedEntityMode('private');
    $rootScope.$digest();
    expect(storageServer.updateSharedExpMode).toHaveBeenCalled();
    expect(console.error).toHaveBeenCalled();
  });

  it('should not delete in private and public option', function() {
    expSharingService.model = { entitySharingMode: 'Public' };
    expSharingService.deleteSharedEntityUser('user1');
    expect(storageServer.deleteSharedExpUser).not.toHaveBeenCalled();
  });

  it('should delete a sharing user', function() {
    expSharingService.model = {
      entitySharingMode: 'Private',
      entityModeSharedOption: 'Private'
    };
    expSharingService.deleteSharedEntityUser('user1');
    $rootScope.$digest();
    expect(storageServer.deleteSharedExpUser).toHaveBeenCalled();
  });

  it('should not delete a sharing user', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    expSharingService.model = {
      entitySharingMode: 'Private',
      entityModeSharedOption: 'Private'
    };
    expSharingService.deleteSharedEntityUser('user1');
    $rootScope.$digest();
    expect(console.error).toHaveBeenCalled();
  });

  it('should search user change', function() {
    expSharingService.model = {
      allUsers: [
        {
          displayName: 'user0',
          id: 'user0',
          username: 'user0'
        }
      ]
    };
    expSharingService.searchUserChange('user0');
    expect(expSharingService.model.filteredUsers[0].id).toBe('user0');
  });

  it('should get shared entity users', function() {
    expSharingService.model = {
      allUsers: [
        {
          displayName: 'user0',
          id: 'user0',
          username: 'user0'
        }
      ]
    };
    expSharingService.getSharedEntityUsers('user0');
    $rootScope.$digest();
    expect(expSharingService.model.sharingUsers[0].id).toBe('user0');
  });

  it('should not get shared entity users', function() {
    expSharingService.model = {
      allUsers: [
        {
          displayName: 'user0',
          id: 'user0',
          username: 'user0'
        }
      ]
    };
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    expSharingService.getSharedEntityUsers('user0');
    $rootScope.$digest();
    expect(console.error).toHaveBeenCalled();
  });

  it('should get the correct Experiment sharing option', function() {
    expSharingService.getSharedEntityMode();
    $rootScope.$digest();
    expect(expSharingService.model.entitySharingMode).toBe('Private');
  });

  it('should not get the correct Model sharing option', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    expSharingService.getSharedEntityMode();
    $rootScope.$digest();
    expect(console.error).toHaveBeenCalled();
  });

  it('should update sharing user list', function() {
    expSharingService.model = {
      allUsers: [
        {
          displayName: 'user1',
          id: 'user1',
          username: 'user1'
        }
      ]
    };
    spyOn(expSharingService, 'getSharedEntityUsers').and.callThrough();
    expSharingService.selectedUserChange({ searchUser: 'user1' });
    $rootScope.$digest();
    expect(storageServer.addSharedExpUsers).toHaveBeenCalled();
  });

  it('should not update sharing user list', function() {
    spyOn(console, 'error');
    storageServer.itWasSuccessful = false;
    expSharingService.model = {
      allUsers: [
        {
          displayName: 'user1',
          id: 'user1',
          username: 'user1'
        }
      ]
    };
    spyOn(expSharingService, 'getSharedEntityUsers').and.callThrough();
    expSharingService.selectedUserChange({ searchUser: 'user1' });
    $rootScope.$digest();
    expect(storageServer.addSharedExpUsers).toHaveBeenCalled();
    expect(console.error).toHaveBeenCalled();
  });
});
