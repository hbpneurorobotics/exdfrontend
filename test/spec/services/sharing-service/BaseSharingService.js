'use strict';

describe('Services: BaseSharingService', function() {
  let nrpModalService;

  beforeEach(module('storageServerMock'));
  beforeEach(module('nrpModalServiceMock'));
  beforeEach(
    inject(function(_nrpModalService_) {
      nrpModalService = _nrpModalService_;
    })
  );
  it('should throw if instantiated', function() {
    expect(function() {
      new window.BaseSharingService();
    }).toThrow();
    expect(function() {
      window.BaseSharingService();
    }).toThrow();
  });

  it('should throw if abstract functions are not overriden', function() {
    var ExperimentsSharingService = function() {};
    ExperimentsSharingService.prototype = Object.create(
      window.BaseSharingService.prototype
    );
    ExperimentsSharingService.prototype.constructor = window.BaseSharingService;
    var abstractFunctions = [
      'updateSharedEntityMode',
      'selectedUserChange',
      'getSharedEntityMode',
      'getSharedEntityUsers',
      'deleteSharedEntityUser'
    ];

    abstractFunctions.forEach(function(fnName) {
      expect(function() {
        new ExperimentsSharingService()[fnName]();
      }).toThrow();
    });
  });

  it('should launch sharing window', function() {
    var ExperimentsSharingService = function() {};
    ExperimentsSharingService.prototype = Object.create(
      window.BaseSharingService.prototype
    );
    ExperimentsSharingService.prototype.constructor = window.BaseSharingService;

    var experimentsSharingService = new ExperimentsSharingService();
    var scope = {
      model: { entitySelected: 'entitySelected', entityType: 'entityType' }
    };
    experimentsSharingService.nrpModalService = nrpModalService;
    experimentsSharingService.model = {
      sharingUsers: [],
      entitySharingMode: 'private',
      allUsers: [],
      entityModeSharedOption: 'Shared',
      entitySelected: '',
      entityType: ''
    };
    experimentsSharingService.launchSharedEntityWindow(
      'entityId',
      scope,
      'entityType'
    );
    expect(nrpModalService.createModal).toHaveBeenCalled();
  });

  it('should return the proper user from the user list', function() {
    var ExperimentsSharingService = function() {};
    ExperimentsSharingService.prototype = Object.create(
      window.BaseSharingService.prototype
    );
    ExperimentsSharingService.prototype.constructor = window.BaseSharingService;

    var experimentsSharingService = new ExperimentsSharingService();
    experimentsSharingService.nrpModalService = nrpModalService;
    experimentsSharingService.model = {
      sharingUsers: [],
      entitySharingMode: 'private',
      allUsers: [
        {
          displayName: 'user1',
          id: 'user1',
          username: 'user1'
        }
      ],
      entityModeSharedOption: 'Shared',
      entitySelected: '',
      entityType: ''
    };

    experimentsSharingService.searchUserChange('user1');
    expect(experimentsSharingService.model.filteredUsers[0].id).toBe('user1');
  });
});
