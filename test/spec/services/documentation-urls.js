'use strict';

describe('Services: documentation-urls', function() {
  var documentationURLs;
  beforeEach(module('exdFrontendApp'));
  beforeEach(
    inject(function(_documentationURLs_) {
      documentationURLs = _documentationURLs_;
    })
  );

  it('should provide the correct help urls', function() {
    var docUrls = documentationURLs.getDocumentationURLs();
    expect(docUrls).toEqual({
      cleDocumentationURL:
        'https://neurorobotics.net/Documentation/3.2.x/latest/nrp/modules/CLE/hbp_nrp_cle',
      backendDocumentationURL:
        'https://neurorobotics.net/Documentation/3.2.x/latest/nrp/modules/ExDBackend/hbp_nrp_backend',
      platformDocumentationURL:
        'https://neurorobotics.net/Documentation/3.2.x/latest/nrp'
    });
  });
});
