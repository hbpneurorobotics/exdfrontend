/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END **/
(function() {
  'use strict';

  class StorageServerRequestInterceptor {
    constructor(authService, $window, $q, $location) {
      this.authService = authService;
      this.$window = $window;
      this.$q = $q;
      this.$location = $location;

      this.request = requestConfig => this.requestFn(requestConfig);
      this.responseError = err => this.responseErrorFn(err);
    }

    /*requestFn(requestConfig) {
      return new Promise(resolve => {
        this.authService.initialized.then(() => {
          var token = this.authService.getToken();
          requestConfig.headers.Authorization = 'Bearer ' + token;
          resolve(requestConfig);
        });
      });
    }*/

    requestFn(requestConfig) {
      if (this.authService.initialized === true) {
        var token = this.authService.getToken();
        requestConfig.headers.Authorization = 'Bearer ' + token;
      }
      return requestConfig;
    }

    responseErrorFn(err) {
      if (err.status === 477) {
        // authentication token missing
        this.authService.authenticate(err.data);
      } else if (err.status === 478) {
        this.$location.path('maintenance');
        return this.$q.resolve({});
      }
      return this.$q.reject(err);
    }
  }

  angular
    .module('storageServer')
    .factory('storageServerRequestInterceptor', [
      'authService',
      '$window',
      '$q',
      '$location',
      (...args) => new StorageServerRequestInterceptor(...args)
    ])
    .config([
      '$httpProvider',
      $httpProvider =>
        $httpProvider.interceptors.push('storageServerRequestInterceptor')
    ]);
})();
