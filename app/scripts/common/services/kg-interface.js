/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END **/
(function() {
  'use strict';

  class KgInterfaceService {
    constructor($resource, $stateParams, bbpConfig) {
      this.$resource = $resource;
      this.$stateParams = $stateParams;
      this.bbpConfig = bbpConfig;
      this.KG_URL = bbpConfig.get('api.kg.url');
      this.KG_EDITOR_URL = bbpConfig.get('api.kg.editor-url');
      this.buildKgInterfaceResource();
    }

    buildKgInterfaceResource() {
      let buildAction = action =>
        angular.merge(action, {
          headers: { 'Context-Id': () => this.$stateParams.ctx }
        });

      this.kgRsc = this.$resource(
        this.KG_EDITOR_URL,
        {},
        {
          postKgSimulation: buildAction({
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            url: `${this.KG_EDITOR_URL}/core/simulation/v1.0.0`
          }),
          putKgSimulation: buildAction({
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            url: `${this.KG_EDITOR_URL}/core/simulation/v1.0.0/:id`
          }),
          postKgArtifact: buildAction({
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            url: `${this.KG_EDITOR_URL}/artifacts/:type/v1.0.0`
          }),
          putKgArtifact: buildAction({
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            url: `${this.KG_EDITOR_URL}/artifacts/:type/v1.0.0/:id`
          })
        }
      );
    }

    postKgSimulation(body) {
      return this.kgRsc.postKgSimulation(body).$promise;
    }

    updateKgSimulation(id, rev, body) {
      return this.kgRsc.putKgSimulation({ id, rev }, body).$promise;
    }

    postKgArtifact(type, body) {
      return this.kgRsc.postKgArtifact({ type }, body).$promise;
    }

    updateKgArtifact(type, id, rev, body) {
      return this.kgRsc.putKgArtifact({ type, id, rev }, body).$promise;
    }
  }

  KgInterfaceService.$inject = ['$resource', '$stateParams', 'bbpConfig'];

  angular
    .module('kgInterfaceService', ['ngResource', 'bbpConfig', 'ui.router'])
    .service('kgInterfaceService', KgInterfaceService);
})();
