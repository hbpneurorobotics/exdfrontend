/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END **/
(function() {
  'use strict';

  class StorageServer {
    constructor(
      $resource,
      $window,
      $q,
      $stateParams,
      bbpConfig,
      authService,
      newExperimentProxyService
    ) {
      this.$resource = $resource;
      this.$window = $window;
      this.$q = $q;
      this.$stateParams = $stateParams;
      this.authService = authService;
      this.bbpConfig = bbpConfig;
      this.newExperimentProxyService = newExperimentProxyService;

      this.CLIENT_ID = bbpConfig.get('auth.clientId');
      this.PROXY_URL = bbpConfig.get('api.proxy.url');
      this.STORAGE_BASE_URL = `${this.PROXY_URL}/storage`;
      this.PRODUCTION_PROXY_URL = bbpConfig.get('api.proxy.productionUrl');
      this.EXPERIMENT_BASE_URL = `${this.PROXY_URL}/experiment`;
      this.IDENTITY_BASE_URL = `${this.PROXY_URL}/identity`;
      this.MODELS_URL = `/models`;

      this.buildStorageResource();
    }

    buildStorageResource() {
      const FILE_REGEXP = /attachment; filename=(.*)/;
      let buildAction = action =>
        angular.merge(action, {
          headers: { 'Context-Id': () => this.$stateParams.ctx }
        });

      let transformFileResponse = (data, header) => {
        let uuid = header('uuid');
        let filename = header('content-disposition');
        filename = filename && FILE_REGEXP.exec(filename);
        filename = filename && filename[1];
        return {
          found: !!uuid,
          uuid,
          filename,
          data
        };
      };

      this.proxyRsc = this.$resource(
        this.STORAGE_BASE_URL,
        {},
        {
          getExperiments: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/experiments`
          }),
          getFile: buildAction({
            method: 'GET',
            transformResponse: transformFileResponse,
            url: `${this.STORAGE_BASE_URL}/:experimentId/:filename`,
            responseType: 'text'
          }),
          getUserInfo: buildAction({
            method: 'GET',
            url: `${this.IDENTITY_BASE_URL}/:userid`
          }),
          getGdprStatus: buildAction({
            method: 'GET',
            url: `${this.IDENTITY_BASE_URL}/gdpr`
          }),
          setGdprStatus: buildAction({
            method: 'POST',
            url: `${this.IDENTITY_BASE_URL}/gdpr`
          }),
          getCurrentUserGroups: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.IDENTITY_BASE_URL}/me/groups`
          }),
          getExperimentFiles: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/:experimentId`
          }),
          getBlob: buildAction({
            method: 'GET',
            responseType: 'blob',
            transformResponse: transformFileResponse,
            url: `${this.STORAGE_BASE_URL}/:experimentId/:filename`
          }),
          deleteFile: buildAction({
            method: 'DELETE',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/:experimentId/:filename`
          }),
          deleteExperiment: buildAction({
            method: 'DELETE',
            url: `${this.STORAGE_BASE_URL}/:experimentId`
          }),
          setFile: buildAction({
            method: 'POST',
            headers: { 'Content-Type': 'text/plain' },
            url: `${this.STORAGE_BASE_URL}/:experimentId/:filename`
          }),
          setBlob: buildAction({
            method: 'POST',
            headers: { 'Content-Type': 'application/octet-stream' },
            url: `${this.STORAGE_BASE_URL}/:experimentId/:filename`,
            transformRequest: []
          }),
          setClonedExperiment: buildAction({
            method: 'POST',
            url: `${this.STORAGE_BASE_URL}/clone/:experiment`,
            transformRequest: []
          }),
          getModelsbyType: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/models/:modelType`
          }),
          getCustomModelsByUser: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/models/user/:modelType`
          }),
          getAllModels: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/models/all/:modelType`
          }),
          getCustomSharedModels: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.STORAGE_BASE_URL}/models/shared/:modelType`
          }),
          cloneTemplate: buildAction({
            method: 'POST',
            url: `${this.STORAGE_BASE_URL}/clone/`
          }),
          cloneNew: buildAction({
            method: 'POST',
            url: `${this.STORAGE_BASE_URL}/clonenew/`
          }),
          getMaintenanceMode: buildAction({
            method: 'GET',
            url: `${this.PROXY_URL}/maintenancemode`
          }),
          deleteCustomModel: buildAction({
            method: 'DELETE',
            url: `${this.STORAGE_BASE_URL}/models/:modelType/:modelName`
          }),
          logActivity: buildAction({
            method: 'POST',
            url: `${this.PROXY_URL}/activity_log/:activity`,
            transformRequest: []
          }),
          getBrain: buildAction({
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/brain`
          }),
          saveBrain: buildAction({
            method: 'PUT',
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/brain`,
            transformRequest: []
          }),
          getStateMachines: buildAction({
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/stateMachines`
          }),
          saveStateMachines: buildAction({
            method: 'PUT',
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/stateMachines`,
            transformRequest: []
          }),
          getTransferFunctions: buildAction({
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/transferFunctions`
          }),
          saveTransferFunctions: buildAction({
            method: 'PUT',
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/transferFunctions`,
            transformRequest: []
          }),
          getExperimentConfig: buildAction({
            method: 'GET',
            url: `${this.EXPERIMENT_BASE_URL}/:experimentId/config`
          }),
          getAllUsers: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this.IDENTITY_BASE_URL}/me/users`
          }),
          updateSharedExpMode: buildAction({
            method: 'POST',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/experiments/:experimentId/sharingmode/:sharingMode`
          }),
          updateSharedModelMode: buildAction({
            method: 'POST',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/models/:modelType/:modelId/sharingmode/:sharingMode`
          }),
          addSharedModelUsers: buildAction({
            method: 'POST',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/models/:modelType/:modelId/sharing/:userId`
          }),
          deleteSharedModelUser: buildAction({
            method: 'DELETE',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/models/:modelType/:modelId/sharingusers/:userId`
          }),
          getSharedModelMode: buildAction({
            method: 'GET',
            isArray: false,
            url: `${this
              .STORAGE_BASE_URL}/models/:modelType/:modelId/sharingmode`
          }),
          getSharedModelUsers: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/models/:modelType/:modelId/sharingusers`
          }),
          addSharedExpUsers: buildAction({
            method: 'POST',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/experiments/:experimentId/sharingusers/:userId`
          }),
          deleteSharedExpUser: buildAction({
            method: 'DELETE',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/experiments/:experimentId/sharingusers/:userId`
          }),
          getSharedExpMode: buildAction({
            method: 'GET',
            isArray: false,
            url: `${this
              .STORAGE_BASE_URL}/experiments/:experimentId/sharingmode`
          }),
          getSharedExpUsers: buildAction({
            method: 'GET',
            isArray: true,
            url: `${this
              .STORAGE_BASE_URL}/experiments/:experimentId/sharingusers`
          }),
          setCustomModel: buildAction({
            method: 'POST',
            headers: { 'Content-Type': 'application/octet-stream' },
            url: `${this.STORAGE_BASE_URL}/models/:modelType/:modelName`,
            transformRequest: []
          }),
          importExperiment: buildAction({
            headers: { 'Content-Type': 'application/octet-stream' },
            method: 'POST',
            url: `${this.STORAGE_BASE_URL}/importExperiment`,
            transformRequest: angular.identity
          }),
          scanStorage: buildAction({
            method: 'POST',
            url: `${this.STORAGE_BASE_URL}/scanStorage`
          }),
          getKnowledgeGraphBrains: buildAction({
            method: 'GET',
            url: `${this.STORAGE_BASE_URL}/knowledgeGraphBrains/:query`,
            isArray: true
          }),
          getExperimentZips: buildAction({
            method: 'GET',
            url: `${this.STORAGE_BASE_URL}/experiments/:experimentId/zip`
          }),
          getZip: buildAction({
            method: 'GET',
            headers: { 'Content-Type': 'application/zip' },
            responseType: 'arraybuffer',
            transformResponse: function(data) {
              // Stores the ArrayBuffer object in a property called "data"
              return { data: data };
            },
            url: `${this.STORAGE_BASE_URL}/zip`
          }),
          saveKgAttachment: buildAction({
            method: 'PUT',
            headers: { 'Content-Type': 'application/octet-stream' },
            url: `${this
              .PRODUCTION_PROXY_URL}/storage/knowledgeGraph/data/:filename`
          })
        }
      );
    }

    authServiceInitWrap(fn) {
      return this.authService.promiseInitialized.then(() => {
        return fn();
      });
    }

    getModelsbyType(modelType) {
      return this.proxyRsc.getModelsbyType({ modelType }).$promise;
    }

    getAllModels(modelType) {
      return this.proxyRsc.getAllModels({ modelType }).$promise;
    }

    getCustomModelsByUser(modelType) {
      return this.proxyRsc.getCustomModelsByUser({ modelType }).$promise;
    }

    getCustomSharedModels(modelType) {
      return this.proxyRsc.getCustomSharedModels({ modelType }).$promise;
    }

    setCustomModel(modelType, modelName, fileContent, override) {
      return this.proxyRsc.setCustomModel(
        { modelType, modelName, override },
        fileContent
      ).$promise;
    }

    deleteCustomModel(modelType, modelName) {
      return this.proxyRsc.deleteCustomModel({ modelType, modelName }).$promise;
    }

    updateSharedExpMode(experimentId, sharingMode) {
      return this.proxyRsc.updateSharedExpMode(
        {
          experimentId,
          sharingMode
        },
        {}
      ).$promise;
    }

    getAllUsers() {
      return this.proxyRsc.getAllUsers().$promise;
    }

    addSharedExpUsers(experimentId, userId) {
      return this.proxyRsc.addSharedExpUsers({ experimentId, userId }, {})
        .$promise;
    }

    deleteSharedExpUser(experimentId, userId) {
      return this.proxyRsc.deleteSharedExpUser({ experimentId, userId })
        .$promise;
    }

    getSharedExpUsers(experimentId) {
      return this.proxyRsc.getSharedExpUsers({ experimentId }).$promise;
    }

    getSharedExpMode(experimentId) {
      return this.proxyRsc.getSharedExpMode({ experimentId }).$promise;
    }

    addSharedModelUsers(modelType, modelId, userId) {
      return this.proxyRsc.addSharedModelUsers(
        { modelType, modelId, userId },
        {}
      ).$promise;
    }

    deleteSharedModelUser(modelType, modelId, userId) {
      return this.proxyRsc.deleteSharedModelUser({
        modelType,
        modelId,
        userId
      }).$promise;
    }

    getSharedModelUsers(modelType, modelId) {
      return this.proxyRsc.getSharedModelUsers({ modelType, modelId }).$promise;
    }

    getSharedModelMode(modelType, modelId) {
      return this.proxyRsc.getSharedModelMode({ modelType, modelId }).$promise;
    }

    updateSharedModelMode(modelType, modelId, sharingMode) {
      return this.proxyRsc.updateSharedModelMode(
        {
          modelType,
          modelId,
          sharingMode
        },
        {}
      ).$promise;
    }

    getExperiments(filter) {
      return this.proxyRsc.getExperiments({ filter }).$promise;
    }

    getExperimentConfig(experimentId) {
      return this.proxyRsc.getExperimentConfig({ experimentId }).$promise;
    }

    getExperimentFiles(experimentId) {
      return this.proxyRsc.getExperimentFiles({ experimentId }).$promise;
    }

    getFileContent(experimentId, filename, byname = false) {
      return this.proxyRsc.getFile({ experimentId, filename, byname }).$promise;
    }

    getBlobContent(experimentId, filename, byname = false) {
      return this.proxyRsc.getBlob({ experimentId, filename, byname }).$promise;
    }

    getBase64Content(experimentId, filename, byname = false) {
      return this.proxyRsc
        .getBlob({ experimentId, filename, byname })
        .$promise.then(response =>
          this.$q((resolve, reject) => {
            let reader = new FileReader();
            reader.addEventListener('loadend', e => {
              if (e.target.result !== 'data:')
                resolve(e.target.result.replace(/data:[^;]*;base64,/g, ''));
              else reject();
            });
            reader.readAsDataURL(response.data);
          })
        );
    }

    deleteEntity(experimentId, filename, byname = false, type = 'file') {
      return this.proxyRsc.deleteFile({ experimentId, filename, byname, type })
        .$promise;
    }

    deleteFile(experimentId, filename, byname = false) {
      return this.deleteEntity(experimentId, filename, byname, 'file');
    }

    deleteFolder(experimentId, folderName, byname = false) {
      return this.deleteEntity(experimentId, folderName, byname, 'folder');
    }

    deleteExperiment(experimentId) {
      return this.proxyRsc.deleteExperiment({ experimentId }).$promise;
    }

    setFileContent(experimentId, filename, fileContent, byname = false) {
      return this.proxyRsc.setFile(
        { experimentId, filename, byname },
        fileContent
      ).$promise;
    }

    createFolder(experimentId, folderName) {
      return this.proxyRsc.setFile(
        { experimentId, filename: folderName, type: 'folder' },
        null
      ).$promise;
    }

    setBlobContent(experimentId, filename, fileContent, byname = false) {
      return this.proxyRsc.setBlob(
        { experimentId, filename, byname },
        new Uint8Array(fileContent)
      ).$promise;
    }

    getCurrentUser() {
      // similarly to the oidc api, the storage server 'identity/me' endpoint returns information about the current user
      return this.authService.promiseInitialized.then(
        () => this.proxyRsc.getUserInfo({ userid: 'me' }).$promise
      );
    }

    getUser(userid) {
      return this.authService.promiseInitialized.then(
        () => this.proxyRsc.getUserInfo({ userid }).$promise
      );
    }

    getGdprStatus() {
      return this.proxyRsc.getGdprStatus().$promise;
    }

    acceptGdpr() {
      return this.proxyRsc.setGdprStatus().$promise;
    }

    getCurrentUserGroups() {
      return this.authService.promiseInitialized.then(
        () => this.proxyRsc.getCurrentUserGroups().$promise
      );
    }

    cloneClonedExperiment(experiment) {
      return this.proxyRsc.setClonedExperiment({ experiment }, null).$promise;
    }

    cloneTemplate(expPath, contextId) {
      return this.proxyRsc.cloneTemplate({ expPath, contextId }).$promise;
    }

    cloneNew(environmentPath, contextId, experimentName, experimentMode) {
      return this.proxyRsc.cloneNew({
        environmentPath,
        contextId,
        experimentName,
        experimentMode
      }).$promise;
    }

    getMaintenanceMode() {
      return this.proxyRsc.getMaintenanceMode().$promise;
    }

    logActivity(activity, logObject) {
      return this.proxyRsc.logActivity({ activity }, JSON.stringify(logObject))
        .$promise;
    }

    getBrain(experimentId) {
      return this.proxyRsc.getBrain({
        experimentId
      }).$promise;
    }

    saveBrain(experimentId, brain, populations, removePopulations, newBrain) {
      return this.proxyRsc.saveBrain(
        { experimentId },
        JSON.stringify({ brain, populations, removePopulations, newBrain })
      ).$promise;
    }

    getStateMachines(experimentId) {
      return this.proxyRsc.getStateMachines({
        experimentId
      }).$promise;
    }

    saveStateMachines(experimentId, stateMachines) {
      return this.proxyRsc.saveStateMachines(
        { experimentId },
        JSON.stringify({ stateMachines })
      ).$promise;
    }

    getTransferFunctions(experimentId) {
      return this.proxyRsc.getTransferFunctions({
        experimentId
      }).$promise;
    }

    saveTransferFunctions(experimentId, transferFunctions) {
      return this.proxyRsc.saveTransferFunctions(
        { experimentId },
        JSON.stringify({ transferFunctions })
      ).$promise;
    }

    getKnowledgeGraphBrains(query) {
      if (query) {
        return new Promise(function(resolve, reject) {
          resolve([]);
          reject('error');
        });
      }
      /*
      The following dead code can be revived by deleting the preceding if statement and commenting out after:
      - the technical account is migrated to Collab 2
      - the Knowledge Graph Brain data is migrated to KG 3
      return this.proxyRsc.getKnowledgeGraphBrains({ query }).$promise;
      */
    }

    getExperimentZips(experimentId) {
      return this.proxyRsc.getExperimentZips({ experimentId }, {}).$promise;
    }

    getZip(path, name) {
      return this.proxyRsc.getZip({ path, name }).$promise;
    }

    getRobotConfigPath(experimentID, robotId = undefined) {
      const demoMode =
        window.bbpConfig.demomode && window.bbpConfig.demomode.demoCarousel;
      if (demoMode) return Promise.reject();

      return this.getFileContent(
        experimentID,
        'experiment_configuration.exc',
        true
      )
        .then(exc => {
          const xml = $.parseXML(exc.data);
          const bibiConf = xml.getElementsByTagNameNS('*', 'bibiConf')[0];

          return this.getFileContent(
            experimentID,
            bibiConf.attributes.src.value,
            true
          );
        })
        .then(bibi => {
          const xml = $.parseXML(bibi.data);
          let bodyModel = {};

          if (robotId) {
            //look for a specific robot in the experiment
            bodyModel = [
              ...xml.getElementsByTagNameNS('*', 'bodyModel')
            ].filter(bd => bd.attributes.robotId.value == robotId)[0];
          } else {
            //use the default robot of the experiment
            bodyModel = xml.getElementsByTagNameNS('*', 'bodyModel')[0];
          }

          if (!bodyModel) return null;
          if (bodyModel.attributes.model) {
            return `${this.STORAGE_BASE_URL}/models/robots/${bodyModel
              .attributes.model.value}/config`;
          } else {
            // robot is local
            return `${this.STORAGE_BASE_URL}/models/robots/${bodyModel
              .attributes.robotId.value}/config`;
          }
        });
    }

    importExperiment(zip) {
      return this.proxyRsc.importExperiment({}, zip).$promise;
    }

    scanStorage() {
      return this.proxyRsc.scanStorage().$promise;
    }

    saveKgAttachment(filename, body) {
      return this.proxyRsc.saveKgAttachment({ filename }, body).$promise;
    }
  }

  StorageServer.$inject = [
    '$resource',
    '$window',
    '$q',
    '$stateParams',
    'bbpConfig',
    'authService',
    'newExperimentProxyService'
  ];

  /*class StorageServerTokenManager {
    constructor($location, bbpConfig) {
      this.$location = $location;
      this.CLIENT_ID = bbpConfig.get('auth.clientId');
      this.STORAGE_KEY = `tokens-${this
        .CLIENT_ID}@https://iam.ebrains.eu/auth/realms/hbp`;

      //this.checkForNewTokenToStore();
    }

    checkForNewTokenToStore() {
      console.info('checkForNewTokenToStore');
      const path = this.$location.url();
      const accessTokenMatch = /&access_token=([^&]*)/.exec(path);

      if (!accessTokenMatch || !accessTokenMatch[1]) return;

      let accessToken = accessTokenMatch[1];

      localStorage.setItem(
        this.STORAGE_KEY,
        //eslint-disable-next-line camelcase
        JSON.stringify([{ access_token: accessToken }])
      );
      const pathMinusAccessToken = path.substr(
        0,
        path.indexOf('&access_token=')
      );
      this.$location.url(pathMinusAccessToken);

      // Clean up session state
      const sessionStateMatch = /&session_state=([^&]*)/.exec(
        pathMinusAccessToken
      );
      if (!sessionStateMatch || !sessionStateMatch[1]) return;
      const pathMinusSessionStaten = path.substr(
        0,
        path.indexOf('&session_state=')
      );
      this.$location.url(pathMinusSessionStaten);
    }

    clearStoredToken() {
      localStorage.removeItem(this.STORAGE_KEY);
    }

    getStoredToken() {
      let storedItem = localStorage.getItem(this.STORAGE_KEY);
      if (!storedItem) {
        // this token will be rejected by the server and the client will get a proper auth error
        return 'no-token';
      }

      try {
        let tokens = JSON.parse(storedItem);
        return tokens.length ? tokens[tokens.length - 1].access_token : null;
      } catch (e) {
        // this token will be rejected by the server and the client will get a proper auth error
        return 'malformed-token';
      }
    }
  }

  StorageServerTokenManager.$$ngIsClass = true;
  StorageServerTokenManager.$inject = ['$location', 'bbpConfig'];*/

  angular
    .module('storageServer', [
      'ngResource',
      'bbpConfig',
      'ui.router',
      'newExperiment'
    ])
    .service('storageServer', StorageServer);
  //.service('storageServerTokenManager', StorageServerTokenManager);
})();
