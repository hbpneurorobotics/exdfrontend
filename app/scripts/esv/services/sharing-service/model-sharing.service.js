/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/

(function() {
  'use strict';

  /*global BaseSharingService */
  class ModelSharingService extends BaseSharingService {
    constructor(
      $q,
      nrpModalService,
      storageServer,
      nrpUser,
      nrpErrorDialog,
      $http,
      bbpConfig
    ) {
      super(
        $q,
        nrpModalService,
        storageServer,
        nrpUser,
        nrpErrorDialog,
        $http,
        bbpConfig
      );
    }

    updateSharedEntityMode() {
      this.storageServer
        .updateSharedModelMode(
          this.model.entityType,
          this.model.entitySelected,
          this.model.entitySharingMode
        )
        .catch(err =>
          console.error(
            `Failed  updating of the sharing-mode in the model :\n${err}`
          )
        );
    }

    selectedUserChange(search) {
      if (
        this.model.allUsers.map(e => e.displayName).includes(search.searchUser)
      ) {
        this.storageServer
          .addSharedModelUsers(
            this.model.entityType,
            this.model.entitySelected,
            search.searchUser
          )
          .then(() => {
            search.searchUser = '';
            return this.getSharedEntityUsers();
          })
          .catch(err =>
            console.error(
              `Failed to add a sharing user into the model :\n${err}`
            )
          );
      }
    }

    getSharedEntityMode() {
      this.storageServer
        .getSharedModelMode(this.model.entityType, this.model.entitySelected)
        .then(response => {
          this.model.entitySharingMode = response.sharingOption;
        })
        .catch(err =>
          console.error(`Failed getting the sharing-mode of the model:\n${err}`)
        );
    }

    getSharedEntityUsers() {
      this.storageServer
        .getSharedModelUsers(this.model.entityType, this.model.entitySelected)
        .then(users => {
          this.model.sharingUsers = [];
          users.forEach(user => {
            this.model.sharingUsers.push(
              this.model.allUsers.filter(e => e.id === user)[0]
            );
          });
        })
        .catch(err =>
          console.error(`Failed to get the users sharing of the model:\n${err}`)
        );
    }

    deleteSharedEntityUser(user) {
      if (this.model.entitySharingMode != this.model.entityModeSharedOption)
        return;

      this.storageServer
        .deleteSharedModelUser(
          this.model.entityType,
          this.model.entitySelected,
          user
        )
        .then(() => this.getSharedEntityUsers())
        .catch(err => console.error(`Failed deleting a sharing user:\n${err}`));
    }
  }
  ModelSharingService.$$ngIsClass = true;
  ModelSharingService.$inject = [
    '$q',
    'nrpModalService',
    'storageServer',
    'nrpUser',
    'nrpErrorDialog',
    '$http',
    'bbpConfig'
  ];
  angular
    .module('exdFrontendApp')
    .service('modelSharingService', ModelSharingService);
})();
