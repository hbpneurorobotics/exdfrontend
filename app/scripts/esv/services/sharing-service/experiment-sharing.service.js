/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/

(function() {
  'use strict';

  /*global BaseSharingService */
  class ExperimentSharingService extends BaseSharingService {
    constructor(
      $q,
      nrpModalService,
      storageServer,
      nrpUser,
      nrpErrorDialog,
      $http,
      bbpConfig
    ) {
      super(
        $q,
        nrpModalService,
        storageServer,
        nrpUser,
        nrpErrorDialog,
        $http,
        bbpConfig
      );
    }
    updateSharedEntityMode() {
      this.storageServer
        .updateSharedExpMode(
          this.model.entitySelected,
          this.model.entitySharingMode
        )
        .catch(err =>
          console.error(
            `Failed the updating of the sharing-mode in the experiment :\n${err}`
          )
        );
    }

    selectedUserChange(search) {
      const index = this.model.allUsers
        .map(e => e.displayName)
        .indexOf(search.searchUser);
      if (index >= 0) {
        this.storageServer
          .addSharedExpUsers(
            this.model.entitySelected,
            this.model.allUsers[index].id
          )
          .then(() => {
            search.searchUser = '';
            return this.getSharedEntityUsers();
          })
          .catch(err =>
            console.error(
              `Failed to add a sharing user into the experiment :\n${err}`
            )
          );
      }
    }

    getSharedEntityMode() {
      this.storageServer
        .getSharedExpMode(this.model.entitySelected)
        .then(response => (this.model.entitySharingMode = response.data))
        .catch(err =>
          console.error(
            `Failed getting the sharing-mode of the experiment:\n${err}`
          )
        );
    }

    getSharedEntityUsers() {
      this.storageServer
        .getSharedExpUsers(this.model.entitySelected)
        .then(users => {
          this.model.sharingUsers = [];
          users.forEach(user => {
            this.model.sharingUsers.push(
              this.model.allUsers.filter(e => e.id === user)[0]
            );
          });
        })
        .catch(err =>
          console.error(
            `Failed getting the sharing users of the experiment:\n${err}`
          )
        );
    }

    deleteSharedEntityUser(user) {
      if (this.model.entitySharingMode != this.model.entityModeSharedOption)
        return;
      this.storageServer
        .deleteSharedExpUser(this.model.entitySelected, user)
        .then(() => this.getSharedEntityUsers())
        .catch(err => console.error(`Failed deleting a sharing user:\n${err}`));
    }
  }
  ExperimentSharingService.$$ngIsClass = true;
  ExperimentSharingService.$inject = [
    '$q',
    'nrpModalService',
    'storageServer',
    'nrpUser',
    'nrpErrorDialog',
    '$http',
    'bbpConfig'
  ];
  angular
    .module('exdFrontendApp')
    .service('experimentSharingService', ExperimentSharingService);
})();
