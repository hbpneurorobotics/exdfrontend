/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
(function() {
  'use strict';
  angular.module('experimentList').directive('importExperimentButtons', [
    'userContextService',
    'importExperimentService',
    function(userContextService, importExperimentService) {
      return {
        templateUrl:
          'components/experiment-list/import-experiment-buttons.template.html',
        restrict: 'EA',
        scope: false,
        link: function(scope, element) {
          scope.isImporting = false;
          scope.isLocalUser = userContextService.localUser;
          scope.importFolderPopupClick = function() {
            scope.importFolderResponse = undefined;
          };

          scope.importZipPopupClick = function() {
            scope.importZipResponses = undefined;
          };

          scope.scanStoragePopupClick = function() {
            scope.scanStorageResponse = undefined;
          };

          let importExperimentFolderInput = element.find(
            '#import-experiment-folder-input'
          );

          importExperimentFolderInput.bind('change', e => {
            scope.isImporting = true;
            importExperimentService
              .importExperimentFolder(e)
              .then(response => {
                scope.importFolderResponse = response;
                scope.loadExperiments(true);
                scope.selectExperiment({ id: response.destFolderName });
              })
              .finally(() => {
                importExperimentFolderInput[0].value = ''; // Allows to re-import the same folder
                scope.isImporting = false;
              });
          });

          let importZippedExperimentInput = element.find(
            '#import-zip-experiment-input'
          );

          importZippedExperimentInput.bind('change', e => {
            scope.isImporting = true;
            importExperimentService
              .importZippedExperiment(e)
              .then(responses => {
                scope.importZipResponses = responses;
                scope.loadExperiments(true);
                const lastImportedExperiment = responses.destFolderName
                  .split(',')
                  .pop()
                  .trim();
                scope.selectExperiment({ id: lastImportedExperiment });
              })
              .finally(() => {
                importZippedExperimentInput[0].value = ''; // Allows to re-import the same zip file
                scope.isImporting = false;
              });
          });

          scope.importExperimentFolderClick = function() {
            importExperimentFolderInput.click();
          };

          scope.importZippedExperimentClick = function() {
            importZippedExperimentInput.click();
          };

          scope.scanStorageClick = function() {
            scope.isImporting = true;
            importExperimentService
              .scanStorage()
              .then(response => {
                scope.scanStorageResponse = response;
                scope.loadExperiments(true);
                const lastImportedExperiment = response.addedFolders
                  .split(',')
                  .pop()
                  .trim();
                scope.selectExperiment({ id: lastImportedExperiment });
              })
              .finally(() => {
                scope.isImporting = false;
              });
          };
        }
      };
    }
  ]);
})();
