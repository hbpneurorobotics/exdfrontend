/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
(function() {
  'use strict';

  class TipTooltipService {
    constructor(
      tipCodesFactory,
      tipClustersFactory,
      DEFAULT_TIP_CLUSTER_IDENTIFIER,
      $sce
    ) {
      this.initialize(
        tipCodesFactory,
        tipClustersFactory,
        DEFAULT_TIP_CLUSTER_IDENTIFIER,
        $sce
      );
    }

    initialize(
      tipCodesFactory,
      tipClustersFactory,
      DEFAULT_TIP_CLUSTER_IDENTIFIER,
      $sce
    ) {
      // Indicate to other components that the service is not ready yet.
      this.ready = false;

      // Store general properties.
      this.tipCodesFactory = tipCodesFactory;
      this.tipClustersFactory = tipClustersFactory;
      this.DEFAULT_TIP_CLUSTER_IDENTIFIER = DEFAULT_TIP_CLUSTER_IDENTIFIER;
      this.$sce = $sce;

      this.hidden = false;

      // Get the shared tip codes...
      this.tipCodes = tipCodesFactory.getTipCodes();

      // ... and load the preferences.
      this.loadLocalPreferences();

      // Get the shared tip clusters ...
      this.tipClusters = tipClustersFactory.getTipClusters();
      // ... reset it ...
      _.forEach(this.tipClusters, (tipCluster, tipClusterIdentifier) => {
        if (this.tipClusters.hasOwnProperty(tipClusterIdentifier)) {
          this.tipClusters[tipClusterIdentifier] = undefined;
        }
      });
      // ... and initialize it.
      this.initializeTipClusterIfNotYetExistent(
        this.DEFAULT_TIP_CLUSTER_IDENTIFIER
      );

      this.ready = true;
    }

    /**
     * Resets the service by re-initializing it and resets the tipClusters. The re-initialization is called with the original
     * constructor parameters. Attention: Mutations on them are not reset.
     */
    reset() {
      this.initialize(
        this.tipCodesFactory,
        this.tipClustersFactory,
        this.DEFAULT_TIP_CLUSTER_IDENTIFIER,
        this.$sce
      );
    }

    /**
     * Initializes a tip cluster with the specified identifier if such tip cluster does not already exists.
     * @param {String} identifier 
     */
    initializeTipClusterIfNotYetExistent(identifier) {
      if (this.tipClusters[identifier] === undefined) {
        this.tipClusters[identifier] = {
          tips: []
        };
      }
    }

    /**
     * Loads the local preferences.
     */
    loadLocalPreferences() {
      this.hidden = localStorage.getItem('TIP_TOOLTIP_HIDE') === 'true';

      _.forEach(this.tipCodes, function(tip, tipType) {
        tip.markedAsRead =
          localStorage.getItem('TIP_TOOLTIP_MARKED_AS_READ_' + tipType) ===
          'true';
      });
    }

    /**
     * Sets the local preferences for the specified tip. Returns a promise for this task.
     * @param {*} tip 
     */
    setLocalTipPreferences(tip) {
      localStorage.setItem(
        'TIP_TOOLTIP_MARKED_AS_READ_' + this.tipToType(tip),
        tip.markedAsRead ? 'true' : 'false'
      );
    }

    /**
     * Sets the local hidden preferences for the module. Returns a promise for this task.
     */
    setLocalHiddenPreferences() {
      localStorage.setItem('TIP_TOOLTIP_HIDE', this.hidden ? 'true' : 'false');
    }

    /**
     * Sets the hidden property and automatically updates the local storage item.
     * @param {Boolean} hide 
     */
    setHidden(hide) {
      this.hidden = hide;
      this.setLocalHiddenPreferences();
    }

    /**
     * Returns the corresponding type string of the specified tip if it is present in the current tip codes.
     * Returns an empty string otherwise.
     * @param {Object} tip 
     */
    tipToType(tip) {
      var tipType = '';

      _.forEach(this.tipCodes, function(itip, type) {
        if (itip === tip) tipType = type;
      });

      return tipType;
    }

    /**
     * Set the current tip of the tip cluster with the specified identifier to the specified tip.
     * @param {Object} tip
     * @param {String} tipClusterIdentifier 
     */
    setCurrentTip(
      tip,
      tipClusterIdentifier = this.DEFAULT_TIP_CLUSTER_IDENTIFIER
    ) {
      // Ensure the tip cluster exists.
      this.initializeTipClusterIfNotYetExistent(tipClusterIdentifier);

      // Initialize/reset tip properties.
      tip.tipListPos = 0;
      tip.hidden = false;

      // Add the tip at first position if it is not already included.
      if (this.tipClusters[tipClusterIdentifier].tips.includes(tip) === false) {
        this.tipClusters[tipClusterIdentifier].tips.unshift(tip);
      }
    }

    /**
     * Resets the display properties of all tip code tips. Also resets the hidden property unless otherwise specified.
     */
    resetAllTipCodeTipPreferences() {
      this.setHidden(false);

      // For all tips:
      _.forEach(this.tipCodes, tip => {
        if (
          tip.keepHiddenPropertyOnReset === undefined ||
          tip.keepHiddenPropertyOnReset === false
        ) {
          // Show it.
          tip.hidden = false;
        }

        // Reset its preferences.
        tip.markedAsRead = false;
        this.setLocalTipPreferences(tip);
      });
    }

    /**
     * Should the specified tip be displayed?
     */
    displayTip(tip) {
      let notMarkedOrShowAnyway =
        (tip.markedAsRead !== undefined &&
          tip.markedAsRead &&
          tip.doNotShowAgainWhenMarkedAsRead) === false;
      let notHidden = (tip.hidden !== undefined && tip.hidden) === false;

      return notHidden && notMarkedOrShowAnyway;
    }

    /**
     * Is at least one tip **not** displayed in the tip cluster with the specified identifier?
     * @param {String} tipClusterIdentifier 
     */
    someTipsAreNotDisplayedInTipCluster(
      tipClusterIdentifier = this.DEFAULT_TIP_CLUSTER_IDENTIFIER
    ) {
      if (this.hidden) {
        return true;
      }

      let oneHidden = false;

      _.forEach(this.tipClusters[tipClusterIdentifier].tips, tip => {
        if (this.displayTip(tip) === false) {
          oneHidden = true;
        }
      });

      return oneHidden;
    }

    /**
     * Is at least one tip displayed in the tip cluster with the specified identifier?
     * @param {String} tipClusterIdentifier 
     */
    someTipsAreDisplayedInTipCluster(
      tipClusterIdentifier = this.DEFAULT_TIP_CLUSTER_IDENTIFIER
    ) {
      if (this.hidden) {
        return false;
      }

      let oneDisplayed = false;

      _.forEach(this.tipClusters[tipClusterIdentifier].tips, tip => {
        if (this.displayTip(tip)) {
          oneDisplayed = true;
        }
      });

      return oneDisplayed;
    }
  }

  TipTooltipService.$$ngIsClass = true;
  TipTooltipService.$inject = [
    'tipCodesFactory',
    'tipClustersFactory',
    'DEFAULT_TIP_CLUSTER_IDENTIFIER',
    '$sce'
  ];

  angular
    .module('tipTooltipModule')
    .service('tipTooltipService', TipTooltipService);
})();
