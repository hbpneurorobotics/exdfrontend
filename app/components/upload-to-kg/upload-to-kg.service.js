/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
/* This module is thought to centralize the back-end server settings used for the current simulation.*/

(function() {
  'use strict';

  class uploadToKgService {
    constructor($q, kgInterfaceService, storageServer) {
      this.$q = $q;
      this.kgInterfaceService = kgInterfaceService;
      this.storageServer = storageServer;
      this.simulation = undefined;
      this.uploadedArtifacts = [];
      this.knowledgeGraphBrainId = undefined;
    }

    uploadSimulation(body) {
      if (this.simulation) {
        return this.kgInterfaceService
          .updateKgSimulation(
            this.simulation['@id'].split('/').pop(),
            this.simulation.revision,
            body
          )
          .then(res => {
            this.simulation.revision = res['nxv:rev'];
            return this.simulation;
          });
      } else {
        return this.kgInterfaceService.postKgSimulation(body).then(res => {
          this.simulation = {};
          this.simulation['@id'] = res['@id'].replace('/sp10editor/', '/sp10/'); //Luc: This is a hacky, no solution on KG side is available yet
          this.simulation.revision = res['nxv:rev'];
          return this.simulation;
        });
      }
    }

    uploadArtifact(type, identifier, body) {
      let index = -1;
      let rev = 0;
      this.uploadedArtifacts.forEach(function(v, i) {
        if (v['identifier'] === identifier) {
          index = i;
          rev = v.revision;
        }
      });

      if (index >= 0) {
        const idNumber = this.uploadedArtifacts[index].idNumber;
        return this.kgInterfaceService
          .updateKgArtifact(type, idNumber, rev, body)
          .then(res => {
            this.uploadedArtifacts[index].revision = res['nxv:rev'];
            return res;
          });
      } else {
        return this.kgInterfaceService.postKgArtifact(type, body).then(res => {
          const artifact = {
            idNumber: res['@id'].split('/').pop(),
            identifier: identifier,
            revision: res['nxv:rev'],
            uploadedDate: ''
          };
          this.uploadedArtifacts.push(artifact);
          return res;
        });
      }
    }

    uploadAttachment(type, idNumber, body) {
      let index = -1;

      this.uploadedArtifacts.forEach(function(v, i) {
        if (v.idNumber === idNumber) {
          index = i;
        }
      });

      if (index < 0) {
        return this.$q.reject();
      }

      const filename = idNumber + '.' + type;

      return this.storageServer.saveKgAttachment(filename, body).then(res => {
        this.uploadedArtifacts[index].uploadedDate = moment()
          .utc()
          .format('YYYY-MM-DD HH:mm:ss');
        return res;
      });
    }

    getUploadedDate(identifier) {
      let date = '';

      this.uploadedArtifacts.forEach(function(v) {
        if (v.identifier === identifier) {
          date = v.uploadedDate;
        }
      });

      return date;
    }
  }

  uploadToKgService.$inject = ['$q', 'kgInterfaceService', 'storageServer'];

  angular.module('uploadToKg').service('uploadToKgService', uploadToKgService);
})();
